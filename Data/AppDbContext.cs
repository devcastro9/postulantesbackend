﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Postulantes.Models;

namespace Postulantes.Data;

public partial class AppDbContext : IdentityDbContext<UsuarioNet>
{
    public AppDbContext(DbContextOptions<AppDbContext> options) : base(options) { }

    public virtual DbSet<Beneficiario> Beneficiarios { get; set; }

    public virtual DbSet<Departamento> Departamentos { get; set; }

    public virtual DbSet<EstadoRegistro> EstadoRegistros { get; set; }

    public virtual DbSet<EstudiosRealizado> EstudiosRealizados { get; set; }

    public virtual DbSet<ExperienciaLaboral> ExperienciaLaborales { get; set; }

    public virtual DbSet<GcControles> GcControles { get; set; }

    public virtual DbSet<GcFormulario> GcFormularios { get; set; }

    public virtual DbSet<GcPais> GcPaises { get; set; }

    public virtual DbSet<GcRight> GcRights { get; set; }

    public virtual DbSet<GcRoles> GcRoles { get; set; }

    public virtual DbSet<GcTipoDocumentoId> GcTipoDocumentoIds { get; set; }

    public virtual DbSet<GcUsuario> GcUsuarios { get; set; }

    public virtual DbSet<GcUsuariosRoles> GcUsuariosRoles { get; set; }

    public virtual DbSet<Genero> Generos { get; set; }

    public virtual DbSet<Habilidad> Habilidades { get; set; }

    public virtual DbSet<Idioma> Idiomas { get; set; }

    public virtual DbSet<Puesto> Puestos { get; set; }

    public virtual DbSet<RcEstadoCivil> RcEstadoCiviles { get; set; }

    public virtual DbSet<RcNivelEducacional> RcNivelEducacionales { get; set; }

    public virtual DbSet<UnidadMedidaTiempo> UnidadMedidaTiempos { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        // Metodo base
        base.OnModelCreating(modelBuilder);
        // Api fluent
        modelBuilder.Entity<Beneficiario>(entity =>
        {
            entity.Property(e => e.BeneficiarioClasificacionId).HasDefaultValueSql("((0))");
            entity.Property(e => e.Denominacion).HasDefaultValueSql("('S/N')");
            entity.Property(e => e.DocumentoRespaldoId).HasDefaultValueSql("('C.I')");
            entity.Property(e => e.EstadoCivilId).IsFixedLength();
            entity.Property(e => e.EstadoRegistroId).HasDefaultValueSql("((0))");
            entity.Property(e => e.FechaRegistro).HasDefaultValueSql("(getdate())");
            entity.Property(e => e.GeneroId).HasDefaultValueSql("((0))");
            entity.Property(e => e.Habilitado).HasDefaultValueSql("((1))");
            entity.Property(e => e.Nit).HasDefaultValueSql("((0))");
            entity.Property(e => e.PaisId).HasDefaultValueSql("((31))");
            entity.Property(e => e.PretensionSalarial).HasDefaultValueSql("((0))");
            entity.Property(e => e.UsuarioId).HasDefaultValueSql("((0))");

            entity.HasOne(d => d.Departamento).WithMany(p => p.Beneficiarios).HasConstraintName("FK_Beneficiario_Departamento");

            entity.HasOne(d => d.DocumentoRespaldo).WithMany(p => p.Beneficiarios).HasConstraintName("FK_Beneficiario_gc_tipo_documento_id");

            entity.HasOne(d => d.EstadoCivil).WithMany(p => p.Beneficiarios).HasConstraintName("FK_Beneficiario_rc_estado_civil");

            entity.HasOne(d => d.EstadoRegistro).WithMany(p => p.Beneficiarios).HasConstraintName("FK_Beneficiario_EstadoRegistro");

            entity.HasOne(d => d.Genero).WithMany(p => p.Beneficiarios).HasConstraintName("FK_Beneficiario_Genero");

            entity.HasOne(d => d.Pais).WithMany(p => p.Beneficiarios).HasConstraintName("FK_Beneficiario_gc_pais");

            entity.HasOne(d => d.Puesto).WithMany(p => p.Beneficiarios).HasConstraintName("FK_Beneficiario_Puesto");
        });

        modelBuilder.Entity<EstadoRegistro>(entity =>
        {
            entity.Property(e => e.EstadoCodigoId).IsFixedLength();
        });

        modelBuilder.Entity<EstudiosRealizado>(entity =>
        {
            entity.Property(e => e.EstadoRegistroId).HasDefaultValueSql("((0))");
            entity.Property(e => e.EstudiandoActualmente).HasDefaultValueSql("((0))");
            entity.Property(e => e.FechaRegistro).HasDefaultValueSql("(getdate())");
            entity.Property(e => e.PaisId).HasDefaultValueSql("((31))");

            entity.HasOne(d => d.Beneficiario).WithMany(p => p.EstudiosRealizados).HasConstraintName("FK_EstudiosRealizados_Beneficiario");

            entity.HasOne(d => d.NivelEducativo).WithMany(p => p.EstudiosRealizados).HasConstraintName("FK_EstudiosRealizados_rc_nivel_educacional");

            entity.HasOne(d => d.Pais).WithMany(p => p.EstudiosRealizados).HasConstraintName("FK_EstudiosRealizados_gc_pais");
        });

        modelBuilder.Entity<ExperienciaLaboral>(entity =>
        {
            entity.Property(e => e.EstadoRegistroId).HasDefaultValueSql("((0))");
            entity.Property(e => e.FechaRegistro).HasDefaultValueSql("(getdate())");
            entity.Property(e => e.PaisId).HasDefaultValueSql("((31))");
            entity.Property(e => e.PresentoDocumento).HasDefaultValueSql("((0))");

            entity.HasOne(d => d.Beneficiario).WithMany(p => p.ExperienciaLaborales).HasConstraintName("FK_ExperienciaLaboral_Beneficiario");

            entity.HasOne(d => d.Pais).WithMany(p => p.ExperienciaLaborales).HasConstraintName("FK_ExperienciaLaboral_gc_pais");
        });

        modelBuilder.Entity<GcControles>(entity =>
        {
            entity.HasKey(e => e.CtrlId).HasName("PK_gc_ctrl");

            entity.HasOne(d => d.Form).WithMany(p => p.GcControles).HasConstraintName("FK_gc_controles_gc_formularios");
        });

        modelBuilder.Entity<GcFormulario>(entity =>
        {
            entity.HasKey(e => e.FormId).HasName("PK_gc_forms");

            entity.Property(e => e.FechaSubido).HasDefaultValueSql("(getdate())");
        });

        modelBuilder.Entity<GcPais>(entity =>
        {
            entity.Property(e => e.Id).ValueGeneratedNever();
            entity.Property(e => e.EstadoCodigo).IsFixedLength();
        });

        modelBuilder.Entity<GcRight>(entity =>
        {
            entity.HasOne(d => d.Ctrl).WithMany(p => p.GcRights).HasConstraintName("FK_gc_right_gc_controles");

            entity.HasOne(d => d.IdRoleNavigation).WithMany(p => p.GcRights).HasConstraintName("FK_gc_right_gc_roles");
        });

        modelBuilder.Entity<GcTipoDocumentoId>(entity =>
        {
            entity.Property(e => e.EstadoCodigo).IsFixedLength();
        });

        modelBuilder.Entity<GcUsuario>(entity =>
        {
            entity.Property(e => e.DeptoCodigo).HasDefaultValueSql("((2))");
            entity.Property(e => e.EstadoCodigo)
                .HasDefaultValueSql("('REG')")
                .IsFixedLength();
            entity.Property(e => e.ValidarVersion).HasDefaultValueSql("((1))");
        });

        modelBuilder.Entity<GcUsuariosRoles>(entity =>
        {
            entity.Property(e => e.Fecha).HasDefaultValueSql("(getdate())");

            entity.HasOne(d => d.Role).WithMany(p => p.GcUsuariosRoles)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_gc_usuarios_roles_gc_roles");

            entity.HasOne(d => d.UsrCodigo).WithMany(p => p.GcUsuariosRoles)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_gc_usuarios_roles_gc_usuarios");
        });

        modelBuilder.Entity<Genero>(entity =>
        {
            entity.Property(e => e.EstadoRegistroId).HasDefaultValueSql("((0))");
            entity.Property(e => e.FechaRegistro).HasDefaultValueSql("(getdate())");
            entity.Property(e => e.UsuarioId).HasDefaultValueSql("((0))");
        });

        modelBuilder.Entity<Habilidad>(entity =>
        {
            entity.Property(e => e.EstadoRegistroId).HasDefaultValueSql("((0))");
            entity.Property(e => e.FechaRegistro).HasDefaultValueSql("(getdate())");

            entity.HasOne(d => d.Beneficiario).WithMany(p => p.Habilidades).HasConstraintName("FK_Habilidad_Beneficiario");

            entity.HasOne(d => d.UnidadMedida).WithMany(p => p.Habilidades).HasConstraintName("FK_Habilidad_UnidadMedidaTiempo");
        });

        modelBuilder.Entity<Idioma>(entity =>
        {
            entity.Property(e => e.EstadoRegistroId).HasDefaultValueSql("((0))");
            entity.Property(e => e.FechaRegistro).HasDefaultValueSql("(getdate())");

            entity.HasOne(d => d.Beneficiario).WithMany(p => p.Idiomas).HasConstraintName("FK_Idioma_Beneficiario");
        });

        modelBuilder.Entity<Puesto>(entity =>
        {
            entity.Property(e => e.EmpresaId).HasDefaultValueSql("((1))");
            entity.Property(e => e.EstadoRegistroId).HasDefaultValueSql("((0))");
            entity.Property(e => e.FechaRegistro).HasDefaultValueSql("(getdate())");
            entity.Property(e => e.Publicado).HasDefaultValueSql("((0))");
            entity.Property(e => e.UsuarioId).HasDefaultValueSql("((0))");
            entity.Property(e => e.Vacante).HasDefaultValueSql("((1))");

            entity.HasOne(d => d.Departamento).WithMany(p => p.Puestos).HasConstraintName("FK_Puesto_Departamento");
        });

        modelBuilder.Entity<RcEstadoCivil>(entity =>
        {
            entity.Property(e => e.EstadoCivilCodigo).IsFixedLength();
            entity.Property(e => e.EstadoCodigo).IsFixedLength();
        });

        modelBuilder.Entity<RcNivelEducacional>(entity =>
        {
            entity.Property(e => e.EstadoCodigo).IsFixedLength();
            entity.Property(e => e.Tipo).HasDefaultValueSql("((0))");
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
