﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

#nullable disable

namespace Postulantes.Data.Migrations
{
    /// <inheritdoc />
    public partial class Primeramigracion : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.EnsureSchema(
            //    name: "rrhh");

            migrationBuilder.CreateTable(
                name: "AspNetRoles",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    NormalizedName = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    ConcurrencyStamp = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUsers",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    DepartamentoId = table.Column<int>(type: "int", nullable: false),
                    UserName = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    NormalizedUserName = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    Email = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    NormalizedEmail = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    EmailConfirmed = table.Column<bool>(type: "bit", nullable: false),
                    PasswordHash = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    SecurityStamp = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ConcurrencyStamp = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PhoneNumber = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(type: "bit", nullable: false),
                    TwoFactorEnabled = table.Column<bool>(type: "bit", nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true),
                    LockoutEnabled = table.Column<bool>(type: "bit", nullable: false),
                    AccessFailedCount = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUsers", x => x.Id);
                });

            //migrationBuilder.CreateTable(
            //    name: "Departamento",
            //    schema: "rrhh",
            //    columns: table => new
            //    {
            //        IdDepartamento = table.Column<int>(type: "int", nullable: false)
            //            .Annotation("SqlServer:Identity", "1, 1"),
            //        Descripcion = table.Column<string>(type: "varchar(50)", unicode: false, maxLength: 50, nullable: true),
            //        Habilitado = table.Column<bool>(type: "bit", nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_Departamento", x => x.IdDepartamento);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "EstadoRegistro",
            //    schema: "rrhh",
            //    columns: table => new
            //    {
            //        IdEstadoRegistro = table.Column<int>(type: "int", nullable: false)
            //            .Annotation("SqlServer:Identity", "1, 1"),
            //        EstadoCodigoId = table.Column<string>(type: "char(3)", unicode: false, fixedLength: true, maxLength: 3, nullable: true),
            //        Descripcion = table.Column<string>(type: "varchar(60)", unicode: false, maxLength: 60, nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_EstadoRegistro", x => x.IdEstadoRegistro);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "gc_formularios",
            //    columns: table => new
            //    {
            //        FormId = table.Column<int>(type: "int", nullable: false)
            //            .Annotation("SqlServer:Identity", "1, 1"),
            //        ArchivoForm = table.Column<string>(type: "varchar(150)", unicode: false, maxLength: 150, nullable: true),
            //        NombreForm = table.Column<string>(type: "varchar(50)", unicode: false, maxLength: 50, nullable: true),
            //        Caption = table.Column<string>(type: "varchar(100)", unicode: false, maxLength: 100, nullable: true),
            //        fecha_subido = table.Column<DateTime>(type: "datetime", nullable: true, defaultValueSql: "(getdate())")
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_gc_forms", x => x.FormId);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "gc_pais",
            //    columns: table => new
            //    {
            //        id = table.Column<int>(type: "int", nullable: false),
            //        pais_codigo = table.Column<string>(type: "varchar(3)", unicode: false, maxLength: 3, nullable: false),
            //        pais_descripcion = table.Column<string>(type: "varchar(100)", unicode: false, maxLength: 100, nullable: true),
            //        pais_sigla = table.Column<string>(type: "varchar(10)", unicode: false, maxLength: 10, nullable: true),
            //        pais_cod_telefonico = table.Column<string>(type: "varchar(10)", unicode: false, maxLength: 10, nullable: true),
            //        pais_descripcion_ingles = table.Column<string>(type: "varchar(100)", unicode: false, maxLength: 100, nullable: true),
            //        pais_cod_iso_num = table.Column<string>(type: "varchar(10)", unicode: false, maxLength: 10, nullable: true),
            //        pais_cod_aeropuerto = table.Column<string>(type: "varchar(10)", unicode: false, maxLength: 10, nullable: true),
            //        pais_cod_horario = table.Column<string>(type: "varchar(10)", unicode: false, maxLength: 10, nullable: true),
            //        pais_continente = table.Column<string>(type: "varchar(25)", unicode: false, maxLength: 25, nullable: true),
            //        estado_codigo = table.Column<string>(type: "char(3)", unicode: false, fixedLength: true, maxLength: 3, nullable: true),
            //        fecha_registro = table.Column<DateTime>(type: "datetime", nullable: true),
            //        usr_codigo = table.Column<string>(type: "varchar(20)", unicode: false, maxLength: 20, nullable: true),
            //        hora_registro = table.Column<string>(type: "varchar(8)", unicode: false, maxLength: 8, nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_gc_pais", x => x.id);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "gc_roles",
            //    columns: table => new
            //    {
            //        Id = table.Column<int>(type: "int", nullable: false)
            //            .Annotation("SqlServer:Identity", "1, 1"),
            //        Nombre = table.Column<string>(type: "varchar(100)", unicode: false, maxLength: 100, nullable: true),
            //        NombreNormalizado = table.Column<string>(type: "varchar(100)", unicode: false, maxLength: 100, nullable: true),
            //        CHQ = table.Column<bool>(type: "bit", nullable: true),
            //        LPZ = table.Column<bool>(type: "bit", nullable: true),
            //        CBB = table.Column<bool>(type: "bit", nullable: true),
            //        ORU = table.Column<bool>(type: "bit", nullable: true),
            //        PTS = table.Column<bool>(type: "bit", nullable: true),
            //        TJA = table.Column<bool>(type: "bit", nullable: true),
            //        SCZ = table.Column<bool>(type: "bit", nullable: true),
            //        BEN = table.Column<bool>(type: "bit", nullable: true),
            //        PDO = table.Column<bool>(type: "bit", nullable: true),
            //        EXT = table.Column<bool>(type: "bit", nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_gc_roles", x => x.Id);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "gc_tipo_documento_id",
            //    columns: table => new
            //    {
            //        tipodoc_codigo = table.Column<string>(type: "varchar(3)", unicode: false, maxLength: 3, nullable: false),
            //        tipodoc_descripcion = table.Column<string>(type: "varchar(50)", unicode: false, maxLength: 50, nullable: true),
            //        tipodoc_tipo = table.Column<string>(type: "varchar(1)", unicode: false, maxLength: 1, nullable: true),
            //        TipoDocIdentidadSIN = table.Column<int>(type: "int", nullable: true),
            //        DocIdentidadSIN = table.Column<string>(type: "varchar(3)", unicode: false, maxLength: 3, nullable: true),
            //        estado_codigo = table.Column<string>(type: "char(3)", unicode: false, fixedLength: true, maxLength: 3, nullable: true),
            //        usr_codigo = table.Column<string>(type: "varchar(20)", unicode: false, maxLength: 20, nullable: true),
            //        fecha_registro = table.Column<DateTime>(type: "datetime", nullable: true),
            //        hora_registro = table.Column<string>(type: "varchar(8)", unicode: false, maxLength: 8, nullable: true),
            //        tipodoc_num = table.Column<int>(type: "int", nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_gc_tipo_documento_id", x => x.tipodoc_codigo);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "gc_usuarios",
            //    columns: table => new
            //    {
            //        usr_codigo = table.Column<string>(type: "varchar(20)", unicode: false, maxLength: 20, nullable: false),
            //        beneficiario_codigo = table.Column<string>(type: "varchar(15)", unicode: false, maxLength: 15, nullable: true),
            //        usr_nombres = table.Column<string>(type: "varchar(30)", unicode: false, maxLength: 30, nullable: true),
            //        usr_primer_apellido = table.Column<string>(type: "varchar(30)", unicode: false, maxLength: 30, nullable: true),
            //        usr_segundo_apellido = table.Column<string>(type: "varchar(30)", unicode: false, maxLength: 30, nullable: true),
            //        usr_clave = table.Column<string>(type: "varchar(15)", unicode: false, maxLength: 15, nullable: true),
            //        dgral_codigo = table.Column<string>(type: "varchar(5)", unicode: false, maxLength: 5, nullable: true),
            //        da_codigo = table.Column<string>(type: "varchar(7)", unicode: false, maxLength: 7, nullable: true),
            //        unidad_codigo = table.Column<string>(type: "varchar(15)", unicode: false, maxLength: 15, nullable: true),
            //        ocup_codigo = table.Column<int>(type: "int", nullable: true),
            //        usr_observaciones = table.Column<string>(type: "varchar(60)", unicode: false, maxLength: 60, nullable: true),
            //        idnivelacceso = table.Column<int>(type: "int", nullable: true),
            //        estado_codigo = table.Column<string>(type: "char(3)", unicode: false, fixedLength: true, maxLength: 3, nullable: true, defaultValueSql: "('REG')"),
            //        fecha_registro = table.Column<DateTime>(type: "datetime", nullable: true),
            //        hora_registro = table.Column<string>(type: "varchar(8)", unicode: false, maxLength: 8, nullable: true),
            //        depto_codigo = table.Column<string>(type: "varchar(2)", unicode: false, maxLength: 2, nullable: true, defaultValueSql: "((2))"),
            //        validar_version = table.Column<bool>(type: "bit", nullable: true, defaultValueSql: "((1))")
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_gc_usuarios", x => x.usr_codigo);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "Genero",
            //    schema: "rrhh",
            //    columns: table => new
            //    {
            //        IdGenero = table.Column<int>(type: "int", nullable: false)
            //            .Annotation("SqlServer:Identity", "1, 1"),
            //        Sigla = table.Column<string>(type: "varchar(2)", unicode: false, maxLength: 2, nullable: true),
            //        Descripcion = table.Column<string>(type: "varchar(50)", unicode: false, maxLength: 50, nullable: true),
            //        EstadoRegistroId = table.Column<int>(type: "int", nullable: true, defaultValueSql: "((0))"),
            //        UsuarioId = table.Column<int>(type: "int", nullable: true, defaultValueSql: "((0))"),
            //        FechaRegistro = table.Column<DateTime>(type: "datetime", nullable: true, defaultValueSql: "(getdate())")
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_Genero", x => x.IdGenero);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "Puesto",
            //    schema: "rrhh",
            //    columns: table => new
            //    {
            //        IdPuesto = table.Column<int>(type: "int", nullable: false)
            //            .Annotation("SqlServer:Identity", "1, 1"),
            //        Descripcion = table.Column<string>(type: "varchar(150)", unicode: false, maxLength: 150, nullable: true),
            //        DepartamentoId = table.Column<string>(type: "varchar(2)", unicode: false, maxLength: 2, nullable: true),
            //        EmpresaId = table.Column<int>(type: "int", nullable: true, defaultValueSql: "((1))"),
            //        Vacante = table.Column<bool>(type: "bit", nullable: true, defaultValueSql: "((1))"),
            //        Publicado = table.Column<bool>(type: "bit", nullable: true, defaultValueSql: "((0))"),
            //        EstadoRegistroId = table.Column<int>(type: "int", nullable: true, defaultValueSql: "((0))"),
            //        UsuarioId = table.Column<int>(type: "int", nullable: true, defaultValueSql: "((0))"),
            //        FechaRegistro = table.Column<DateTime>(type: "datetime", nullable: true, defaultValueSql: "(getdate())")
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_Puesto", x => x.IdPuesto);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "rc_estado_civil",
            //    columns: table => new
            //    {
            //        estado_civil_codigo = table.Column<string>(type: "char(1)", unicode: false, fixedLength: true, maxLength: 1, nullable: false),
            //        estado_civil_descripcion = table.Column<string>(type: "varchar(30)", unicode: false, maxLength: 30, nullable: true),
            //        estado_codigo = table.Column<string>(type: "char(3)", unicode: false, fixedLength: true, maxLength: 3, nullable: true),
            //        fecha_registro = table.Column<DateTime>(type: "datetime", nullable: true),
            //        hora_registro = table.Column<string>(type: "varchar(8)", unicode: false, maxLength: 8, nullable: true),
            //        usr_codigo = table.Column<string>(type: "varchar(20)", unicode: false, maxLength: 20, nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_rc_estado_civil", x => x.estado_civil_codigo);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "rc_nivel_educacional",
            //    columns: table => new
            //    {
            //        nivel_educ_codigo = table.Column<int>(type: "int", nullable: false)
            //            .Annotation("SqlServer:Identity", "1, 1"),
            //        nivel_educ_descripcion = table.Column<string>(type: "varchar(50)", unicode: false, maxLength: 50, nullable: true),
            //        tipo = table.Column<int>(type: "int", nullable: true, defaultValueSql: "((0))"),
            //        estado_codigo = table.Column<string>(type: "char(3)", unicode: false, fixedLength: true, maxLength: 3, nullable: true),
            //        usr_codigo = table.Column<string>(type: "varchar(20)", unicode: false, maxLength: 20, nullable: true),
            //        fecha_registro = table.Column<DateTime>(type: "datetime", nullable: true),
            //        hora_registro = table.Column<string>(type: "varchar(8)", unicode: false, maxLength: 8, nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_rc_nivel_educacional", x => x.nivel_educ_codigo);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "UnidadMedidaTiempo",
            //    schema: "rrhh",
            //    columns: table => new
            //    {
            //        IdUnidadMedida = table.Column<int>(type: "int", nullable: false)
            //            .Annotation("SqlServer:Identity", "1, 1"),
            //        Descripcion = table.Column<string>(type: "varchar(50)", unicode: false, maxLength: 50, nullable: true),
            //        Codigo = table.Column<string>(type: "varchar(5)", unicode: false, maxLength: 5, nullable: true),
            //        EstadoRegistroId = table.Column<int>(type: "int", nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_UnidadMedidaTiempo", x => x.IdUnidadMedida);
            //    });

            migrationBuilder.CreateTable(
                name: "AspNetRoleClaims",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    RoleId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    ClaimType = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ClaimValue = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoleClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetRoleClaims_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserClaims",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    ClaimType = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ClaimValue = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUserClaims_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserLogins",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    ProviderKey = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    ProviderDisplayName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UserId = table.Column<string>(type: "nvarchar(450)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserLogins", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_AspNetUserLogins_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserRoles",
                columns: table => new
                {
                    UserId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    RoleId = table.Column<string>(type: "nvarchar(450)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserRoles", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserTokens",
                columns: table => new
                {
                    UserId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    LoginProvider = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Value = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserTokens", x => new { x.UserId, x.LoginProvider, x.Name });
                    table.ForeignKey(
                        name: "FK_AspNetUserTokens_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            //migrationBuilder.CreateTable(
            //    name: "gc_controles",
            //    columns: table => new
            //    {
            //        CtrlId = table.Column<int>(type: "int", nullable: false)
            //            .Annotation("SqlServer:Identity", "1, 1"),
            //        FormId = table.Column<int>(type: "int", nullable: true),
            //        NombreControl = table.Column<string>(type: "varchar(50)", unicode: false, maxLength: 50, nullable: true),
            //        TipoControl = table.Column<string>(type: "varchar(50)", unicode: false, maxLength: 50, nullable: true),
            //        fecha_modificado = table.Column<DateTime>(type: "datetime", nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_gc_ctrl", x => x.CtrlId);
            //        table.ForeignKey(
            //            name: "FK_gc_controles_gc_formularios",
            //            column: x => x.FormId,
            //            principalTable: "gc_formularios",
            //            principalColumn: "FormId");
            //    });

            //migrationBuilder.CreateTable(
            //    name: "gc_usuarios_roles",
            //    columns: table => new
            //    {
            //        usr_codigo_id = table.Column<string>(type: "varchar(20)", unicode: false, maxLength: 20, nullable: false),
            //        RoleId = table.Column<int>(type: "int", nullable: false),
            //        fecha = table.Column<DateTime>(type: "datetime", nullable: true, defaultValueSql: "(getdate())")
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_gc_usuarios_roles", x => new { x.usr_codigo_id, x.RoleId });
            //        table.ForeignKey(
            //            name: "FK_gc_usuarios_roles_gc_roles",
            //            column: x => x.RoleId,
            //            principalTable: "gc_roles",
            //            principalColumn: "Id");
            //        table.ForeignKey(
            //            name: "FK_gc_usuarios_roles_gc_usuarios",
            //            column: x => x.usr_codigo_id,
            //            principalTable: "gc_usuarios",
            //            principalColumn: "usr_codigo");
            //    });

            //migrationBuilder.CreateTable(
            //    name: "Beneficiario",
            //    schema: "rrhh",
            //    columns: table => new
            //    {
            //        IdBeneficiario = table.Column<int>(type: "int", nullable: false)
            //            .Annotation("SqlServer:Identity", "1, 1"),
            //        BeneficiarioClasificacionId = table.Column<int>(type: "int", nullable: true, defaultValueSql: "((0))"),
            //        PuestoId = table.Column<int>(type: "int", nullable: true),
            //        DocumentoIdentidad = table.Column<string>(type: "varchar(20)", unicode: false, maxLength: 20, nullable: true),
            //        DocumentoRespaldoId = table.Column<string>(type: "varchar(3)", unicode: false, maxLength: 3, nullable: true, defaultValueSql: "('C.I')"),
            //        DepartamentoId = table.Column<int>(type: "int", nullable: true),
            //        Iniciales = table.Column<string>(type: "varchar(4)", unicode: false, maxLength: 4, nullable: true),
            //        PrimerApellido = table.Column<string>(type: "varchar(35)", unicode: false, maxLength: 35, nullable: true),
            //        SegundoApellido = table.Column<string>(type: "varchar(35)", unicode: false, maxLength: 35, nullable: true),
            //        ApellidoCasada = table.Column<string>(type: "varchar(30)", unicode: false, maxLength: 30, nullable: true),
            //        Nombres = table.Column<string>(type: "varchar(50)", unicode: false, maxLength: 50, nullable: true),
            //        Nit = table.Column<string>(type: "varchar(20)", unicode: false, maxLength: 20, nullable: true, defaultValueSql: "((0))"),
            //        Denominacion = table.Column<string>(type: "varchar(100)", unicode: false, maxLength: 100, nullable: true, defaultValueSql: "('S/N')"),
            //        FechaNacimiento = table.Column<DateTime>(type: "date", nullable: true),
            //        PaisId = table.Column<int>(type: "int", nullable: true, defaultValueSql: "((31))"),
            //        GeneroId = table.Column<int>(type: "int", nullable: true, defaultValueSql: "((0))"),
            //        EstadoCivilId = table.Column<string>(type: "char(1)", unicode: false, fixedLength: true, maxLength: 1, nullable: true),
            //        TelefonoFijo = table.Column<string>(type: "varchar(30)", unicode: false, maxLength: 30, nullable: true),
            //        TelefonoOficina = table.Column<string>(type: "varchar(30)", unicode: false, maxLength: 30, nullable: true),
            //        TelefonoCelular = table.Column<string>(type: "varchar(30)", unicode: false, maxLength: 30, nullable: true),
            //        EmailPersonal = table.Column<string>(type: "varchar(50)", unicode: false, maxLength: 50, nullable: true),
            //        EmailOficina = table.Column<string>(type: "varchar(50)", unicode: false, maxLength: 50, nullable: true),
            //        DomicilioLegal = table.Column<string>(type: "varchar(250)", unicode: false, maxLength: 250, nullable: true),
            //        LicenciaConducir = table.Column<bool>(type: "bit", nullable: true),
            //        LicenciaMotocicleta = table.Column<bool>(type: "bit", nullable: true),
            //        FotoRuta = table.Column<string>(type: "varchar(150)", unicode: false, maxLength: 150, nullable: true),
            //        EstadoRegistroId = table.Column<int>(type: "int", nullable: true, defaultValueSql: "((0))"),
            //        UsuarioId = table.Column<int>(type: "int", nullable: true, defaultValueSql: "((0))"),
            //        FechaRegistro = table.Column<DateTime>(type: "datetime", nullable: true, defaultValueSql: "(getdate())"),
            //        Habilitado = table.Column<bool>(type: "bit", nullable: true, defaultValueSql: "((1))")
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_Beneficiario", x => x.IdBeneficiario);
            //        table.ForeignKey(
            //            name: "FK_Beneficiario_Departamento",
            //            column: x => x.DepartamentoId,
            //            principalSchema: "rrhh",
            //            principalTable: "Departamento",
            //            principalColumn: "IdDepartamento");
            //        table.ForeignKey(
            //            name: "FK_Beneficiario_EstadoRegistro",
            //            column: x => x.EstadoRegistroId,
            //            principalSchema: "rrhh",
            //            principalTable: "EstadoRegistro",
            //            principalColumn: "IdEstadoRegistro");
            //        table.ForeignKey(
            //            name: "FK_Beneficiario_Genero",
            //            column: x => x.GeneroId,
            //            principalSchema: "rrhh",
            //            principalTable: "Genero",
            //            principalColumn: "IdGenero");
            //        table.ForeignKey(
            //            name: "FK_Beneficiario_Puesto",
            //            column: x => x.PuestoId,
            //            principalSchema: "rrhh",
            //            principalTable: "Puesto",
            //            principalColumn: "IdPuesto");
            //        table.ForeignKey(
            //            name: "FK_Beneficiario_gc_pais",
            //            column: x => x.PaisId,
            //            principalTable: "gc_pais",
            //            principalColumn: "id");
            //        table.ForeignKey(
            //            name: "FK_Beneficiario_gc_tipo_documento_id",
            //            column: x => x.DocumentoRespaldoId,
            //            principalTable: "gc_tipo_documento_id",
            //            principalColumn: "tipodoc_codigo");
            //        table.ForeignKey(
            //            name: "FK_Beneficiario_rc_estado_civil",
            //            column: x => x.EstadoCivilId,
            //            principalTable: "rc_estado_civil",
            //            principalColumn: "estado_civil_codigo");
            //    });

            //migrationBuilder.CreateTable(
            //    name: "gc_right",
            //    columns: table => new
            //    {
            //        RightId = table.Column<int>(type: "int", nullable: false)
            //            .Annotation("SqlServer:Identity", "1, 1"),
            //        IdRole = table.Column<int>(type: "int", nullable: true),
            //        FormId = table.Column<int>(type: "int", nullable: true),
            //        CtrlId = table.Column<int>(type: "int", nullable: true),
            //        Visible = table.Column<bool>(type: "bit", nullable: true),
            //        Enabled = table.Column<bool>(type: "bit", nullable: true),
            //        Locked = table.Column<bool>(type: "bit", nullable: true),
            //        usuario_modificado = table.Column<string>(type: "varchar(20)", unicode: false, maxLength: 20, nullable: false),
            //        fecha_registro = table.Column<DateTime>(type: "datetime", nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_gc_right", x => x.RightId);
            //        table.ForeignKey(
            //            name: "FK_gc_right_gc_controles",
            //            column: x => x.CtrlId,
            //            principalTable: "gc_controles",
            //            principalColumn: "CtrlId");
            //        table.ForeignKey(
            //            name: "FK_gc_right_gc_roles",
            //            column: x => x.IdRole,
            //            principalTable: "gc_roles",
            //            principalColumn: "Id");
            //    });

            //migrationBuilder.CreateTable(
            //    name: "EstudiosRealizados",
            //    schema: "rrhh",
            //    columns: table => new
            //    {
            //        IdEstudiosRealizados = table.Column<int>(type: "int", nullable: false)
            //            .Annotation("SqlServer:Identity", "1, 1"),
            //        BeneficiarioId = table.Column<int>(type: "int", nullable: true),
            //        CarreraOCurso = table.Column<string>(type: "varchar(80)", unicode: false, maxLength: 80, nullable: true),
            //        CentroEducativo = table.Column<string>(type: "varchar(80)", unicode: false, maxLength: 80, nullable: true),
            //        TituloObtenido = table.Column<string>(type: "varchar(80)", unicode: false, maxLength: 80, nullable: true),
            //        NivelEducativoId = table.Column<int>(type: "int", nullable: true),
            //        Tipo = table.Column<int>(type: "int", nullable: true),
            //        Ciudad = table.Column<string>(type: "varchar(50)", unicode: false, maxLength: 50, nullable: true),
            //        PaisId = table.Column<int>(type: "int", nullable: true, defaultValueSql: "((31))"),
            //        FechaInicio = table.Column<DateTime>(type: "date", nullable: true),
            //        FechaFinalizacion = table.Column<DateTime>(type: "date", nullable: true),
            //        EstudiandoActualmente = table.Column<bool>(type: "bit", nullable: true, defaultValueSql: "((0))"),
            //        EstadoRegistroId = table.Column<int>(type: "int", nullable: true, defaultValueSql: "((0))"),
            //        FechaRegistro = table.Column<DateTime>(type: "datetime", nullable: true, defaultValueSql: "(getdate())")
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_EstudiosRealizados", x => x.IdEstudiosRealizados);
            //        table.ForeignKey(
            //            name: "FK_EstudiosRealizados_Beneficiario",
            //            column: x => x.BeneficiarioId,
            //            principalSchema: "rrhh",
            //            principalTable: "Beneficiario",
            //            principalColumn: "IdBeneficiario");
            //        table.ForeignKey(
            //            name: "FK_EstudiosRealizados_gc_pais",
            //            column: x => x.PaisId,
            //            principalTable: "gc_pais",
            //            principalColumn: "id");
            //        table.ForeignKey(
            //            name: "FK_EstudiosRealizados_rc_nivel_educacional",
            //            column: x => x.NivelEducativoId,
            //            principalTable: "rc_nivel_educacional",
            //            principalColumn: "nivel_educ_codigo");
            //    });

            //migrationBuilder.CreateTable(
            //    name: "ExperienciaLaboral",
            //    schema: "rrhh",
            //    columns: table => new
            //    {
            //        IdExperienciaLaboral = table.Column<int>(type: "int", nullable: false)
            //            .Annotation("SqlServer:Identity", "1, 1"),
            //        BeneficiarioId = table.Column<int>(type: "int", nullable: true),
            //        NombreInstitucion = table.Column<string>(type: "varchar(50)", unicode: false, maxLength: 50, nullable: true),
            //        Cargo = table.Column<string>(type: "varchar(50)", unicode: false, maxLength: 50, nullable: true),
            //        FuncionGeneral = table.Column<string>(type: "varchar(500)", unicode: false, maxLength: 500, nullable: true),
            //        PaisId = table.Column<int>(type: "int", nullable: true, defaultValueSql: "((31))"),
            //        Ciudad = table.Column<string>(type: "varchar(50)", unicode: false, maxLength: 50, nullable: true),
            //        FechaInicio = table.Column<DateTime>(type: "date", nullable: true),
            //        FechaFinalizacion = table.Column<DateTime>(type: "date", nullable: true),
            //        PresentoDocumento = table.Column<bool>(type: "bit", nullable: true, defaultValueSql: "((0))"),
            //        EstadoRegistroId = table.Column<int>(type: "int", nullable: true, defaultValueSql: "((0))"),
            //        FechaRegistro = table.Column<DateTime>(type: "datetime", nullable: true, defaultValueSql: "(getdate())")
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_ExperienciaLaboral", x => x.IdExperienciaLaboral);
            //        table.ForeignKey(
            //            name: "FK_ExperienciaLaboral_Beneficiario",
            //            column: x => x.BeneficiarioId,
            //            principalSchema: "rrhh",
            //            principalTable: "Beneficiario",
            //            principalColumn: "IdBeneficiario");
            //        table.ForeignKey(
            //            name: "FK_ExperienciaLaboral_gc_pais",
            //            column: x => x.PaisId,
            //            principalTable: "gc_pais",
            //            principalColumn: "id");
            //    });

            //migrationBuilder.CreateTable(
            //    name: "Habilidad",
            //    schema: "rrhh",
            //    columns: table => new
            //    {
            //        IdHabilidad = table.Column<int>(type: "int", nullable: false)
            //            .Annotation("SqlServer:Identity", "1, 1"),
            //        BeneficiarioId = table.Column<int>(type: "int", nullable: true),
            //        Descripcion = table.Column<string>(type: "varchar(150)", unicode: false, maxLength: 150, nullable: true),
            //        Tiempo = table.Column<int>(type: "int", nullable: true),
            //        UnidadMedidaId = table.Column<int>(type: "int", nullable: true),
            //        EstadoRegistroId = table.Column<int>(type: "int", nullable: true, defaultValueSql: "((0))"),
            //        FechaRegistro = table.Column<DateTime>(type: "datetime", nullable: true, defaultValueSql: "(getdate())")
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_Habilidad", x => x.IdHabilidad);
            //        table.ForeignKey(
            //            name: "FK_Habilidad_Beneficiario",
            //            column: x => x.BeneficiarioId,
            //            principalSchema: "rrhh",
            //            principalTable: "Beneficiario",
            //            principalColumn: "IdBeneficiario");
            //        table.ForeignKey(
            //            name: "FK_Habilidad_UnidadMedidaTiempo",
            //            column: x => x.UnidadMedidaId,
            //            principalSchema: "rrhh",
            //            principalTable: "UnidadMedidaTiempo",
            //            principalColumn: "IdUnidadMedida");
            //    });

            //migrationBuilder.CreateTable(
            //    name: "Idioma",
            //    schema: "rrhh",
            //    columns: table => new
            //    {
            //        IdIdioma = table.Column<int>(type: "int", nullable: false)
            //            .Annotation("SqlServer:Identity", "1, 1"),
            //        BeneficiarioId = table.Column<int>(type: "int", nullable: true),
            //        IdiomaDescripcion = table.Column<string>(type: "varchar(100)", unicode: false, maxLength: 100, nullable: true),
            //        NivelEscrito = table.Column<string>(type: "varchar(150)", unicode: false, maxLength: 150, nullable: true),
            //        NivelOral = table.Column<string>(type: "varchar(150)", unicode: false, maxLength: 150, nullable: true),
            //        NivelLectura = table.Column<string>(type: "varchar(150)", unicode: false, maxLength: 150, nullable: true),
            //        EstadoRegistroId = table.Column<int>(type: "int", nullable: true, defaultValueSql: "((0))"),
            //        FechaRegistro = table.Column<DateTime>(type: "datetime", nullable: true, defaultValueSql: "(getdate())")
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_Idioma", x => x.IdIdioma);
            //        table.ForeignKey(
            //            name: "FK_Idioma_Beneficiario",
            //            column: x => x.BeneficiarioId,
            //            principalSchema: "rrhh",
            //            principalTable: "Beneficiario",
            //            principalColumn: "IdBeneficiario");
            //    });

            migrationBuilder.CreateIndex(
                name: "IX_AspNetRoleClaims_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "AspNetRoles",
                column: "NormalizedName",
                unique: true,
                filter: "[NormalizedName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserClaims_UserId",
                table: "AspNetUserClaims",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserLogins_UserId",
                table: "AspNetUserLogins",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserRoles_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "AspNetUsers",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "AspNetUsers",
                column: "NormalizedUserName",
                unique: true,
                filter: "[NormalizedUserName] IS NOT NULL");

            //migrationBuilder.CreateIndex(
            //    name: "IX_Beneficiario_DepartamentoId",
            //    schema: "rrhh",
            //    table: "Beneficiario",
            //    column: "DepartamentoId");

            //migrationBuilder.CreateIndex(
            //    name: "IX_Beneficiario_DocumentoRespaldoId",
            //    schema: "rrhh",
            //    table: "Beneficiario",
            //    column: "DocumentoRespaldoId");

            //migrationBuilder.CreateIndex(
            //    name: "IX_Beneficiario_EstadoCivilId",
            //    schema: "rrhh",
            //    table: "Beneficiario",
            //    column: "EstadoCivilId");

            //migrationBuilder.CreateIndex(
            //    name: "IX_Beneficiario_EstadoRegistroId",
            //    schema: "rrhh",
            //    table: "Beneficiario",
            //    column: "EstadoRegistroId");

            //migrationBuilder.CreateIndex(
            //    name: "IX_Beneficiario_GeneroId",
            //    schema: "rrhh",
            //    table: "Beneficiario",
            //    column: "GeneroId");

            //migrationBuilder.CreateIndex(
            //    name: "IX_Beneficiario_PaisId",
            //    schema: "rrhh",
            //    table: "Beneficiario",
            //    column: "PaisId");

            //migrationBuilder.CreateIndex(
            //    name: "IX_Beneficiario_PuestoId",
            //    schema: "rrhh",
            //    table: "Beneficiario",
            //    column: "PuestoId");

            //migrationBuilder.CreateIndex(
            //    name: "IX_EstudiosRealizados_BeneficiarioId",
            //    schema: "rrhh",
            //    table: "EstudiosRealizados",
            //    column: "BeneficiarioId");

            //migrationBuilder.CreateIndex(
            //    name: "IX_EstudiosRealizados_NivelEducativoId",
            //    schema: "rrhh",
            //    table: "EstudiosRealizados",
            //    column: "NivelEducativoId");

            //migrationBuilder.CreateIndex(
            //    name: "IX_EstudiosRealizados_PaisId",
            //    schema: "rrhh",
            //    table: "EstudiosRealizados",
            //    column: "PaisId");

            //migrationBuilder.CreateIndex(
            //    name: "IX_ExperienciaLaboral_BeneficiarioId",
            //    schema: "rrhh",
            //    table: "ExperienciaLaboral",
            //    column: "BeneficiarioId");

            //migrationBuilder.CreateIndex(
            //    name: "IX_ExperienciaLaboral_PaisId",
            //    schema: "rrhh",
            //    table: "ExperienciaLaboral",
            //    column: "PaisId");

            //migrationBuilder.CreateIndex(
            //    name: "IX_gc_controles_FormId",
            //    table: "gc_controles",
            //    column: "FormId");

            //migrationBuilder.CreateIndex(
            //    name: "IX_gc_right_CtrlId",
            //    table: "gc_right",
            //    column: "CtrlId");

            //migrationBuilder.CreateIndex(
            //    name: "IX_gc_right_IdRole",
            //    table: "gc_right",
            //    column: "IdRole");

            //migrationBuilder.CreateIndex(
            //    name: "IX_gc_usuarios_roles_RoleId",
            //    table: "gc_usuarios_roles",
            //    column: "RoleId");

            //migrationBuilder.CreateIndex(
            //    name: "IX_Habilidad_BeneficiarioId",
            //    schema: "rrhh",
            //    table: "Habilidad",
            //    column: "BeneficiarioId");

            //migrationBuilder.CreateIndex(
            //    name: "IX_Habilidad_UnidadMedidaId",
            //    schema: "rrhh",
            //    table: "Habilidad",
            //    column: "UnidadMedidaId");

            //migrationBuilder.CreateIndex(
            //    name: "IX_Idioma_BeneficiarioId",
            //    schema: "rrhh",
            //    table: "Idioma",
            //    column: "BeneficiarioId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AspNetRoleClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserLogins");

            migrationBuilder.DropTable(
                name: "AspNetUserRoles");

            migrationBuilder.DropTable(
                name: "AspNetUserTokens");

            //migrationBuilder.DropTable(
            //    name: "EstudiosRealizados",
            //    schema: "rrhh");

            //migrationBuilder.DropTable(
            //    name: "ExperienciaLaboral",
            //    schema: "rrhh");

            //migrationBuilder.DropTable(
            //    name: "gc_right");

            //migrationBuilder.DropTable(
            //    name: "gc_usuarios_roles");

            //migrationBuilder.DropTable(
            //    name: "Habilidad",
            //    schema: "rrhh");

            //migrationBuilder.DropTable(
            //    name: "Idioma",
            //    schema: "rrhh");

            migrationBuilder.DropTable(
                name: "AspNetRoles");

            migrationBuilder.DropTable(
                name: "AspNetUsers");

            //migrationBuilder.DropTable(
            //    name: "rc_nivel_educacional");

            //migrationBuilder.DropTable(
            //    name: "gc_controles");

            //migrationBuilder.DropTable(
            //    name: "gc_roles");

            //migrationBuilder.DropTable(
            //    name: "gc_usuarios");

            //migrationBuilder.DropTable(
            //    name: "UnidadMedidaTiempo",
            //    schema: "rrhh");

            //migrationBuilder.DropTable(
            //    name: "Beneficiario",
            //    schema: "rrhh");

            //migrationBuilder.DropTable(
            //    name: "gc_formularios");

            //migrationBuilder.DropTable(
            //    name: "Departamento",
            //    schema: "rrhh");

            //migrationBuilder.DropTable(
            //    name: "EstadoRegistro",
            //    schema: "rrhh");

            //migrationBuilder.DropTable(
            //    name: "Genero",
            //    schema: "rrhh");

            //migrationBuilder.DropTable(
            //    name: "Puesto",
            //    schema: "rrhh");

            //migrationBuilder.DropTable(
            //    name: "gc_pais");

            //migrationBuilder.DropTable(
            //    name: "gc_tipo_documento_id");

            //migrationBuilder.DropTable(
            //    name: "rc_estado_civil");
        }
    }
}
