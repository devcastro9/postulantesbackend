﻿using System.Text.Json.Serialization;

namespace Postulantes.ViewModels
{
    public class ResponseFile
    {
        [JsonPropertyName("estado")]
        public bool Estado { get; set; }
        [JsonPropertyName("ruta")]
        public string RutaArchivo { get; set; } = null!;
    }
}
