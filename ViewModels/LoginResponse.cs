﻿using System;
using System.Text.Json.Serialization;

namespace Postulantes.ViewModels
{
    public class LoginResponse
    {
        [JsonPropertyName("isLogin")]
        public bool IsLogin { get; set; }
        [JsonPropertyName("jwt")]
        public string? Token { get; set; } = null!;
        [JsonPropertyName("expires")]
        public DateTime Expiracion { get; set; }
        [JsonPropertyName("mensaje")]
        public string Mensaje { get; set; } = null!;
    }
}
