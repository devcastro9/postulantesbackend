﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace Postulantes.ViewModels
{
    public class DataUserModel
    {
        [JsonPropertyName("Id")]
        public string? Id { get; set; }
        [JsonPropertyName("Usuario")]
        public string? UserName { get; set; }
        [JsonPropertyName("Email")]
        public string? Email { get; set; }
        [JsonPropertyName("Departamento")]
        public int DepartamentoId { get; set; }
        [JsonPropertyName("Roles")]
        public ICollection<string> Roles { get; set; } = new List<string>();
    }
}
