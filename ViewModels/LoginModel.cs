﻿using Microsoft.Build.Framework;
using System.Text.Json.Serialization;

namespace Postulantes.ViewModels
{
    public class LoginModel
    {
        [Required]
        [JsonPropertyName("usuario")]
        public string UsuarioCodigo { get; set; } = null!;
        [Required]
        [JsonPropertyName("password")]
        public string Password { get; set; } = null!;
    }
}
