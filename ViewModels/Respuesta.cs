﻿namespace Postulantes.ViewModels
{
    public class Respuesta
    {
        public bool Error { get; set; }
        public string Mensaje { get; set; } = null!;
    }
}
