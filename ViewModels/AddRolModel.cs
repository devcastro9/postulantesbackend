﻿using Microsoft.Build.Framework;
using System.Text.Json.Serialization;

namespace Postulantes.ViewModels
{
    public class AddRolModel
    {
        [Required]
        [JsonPropertyName("usuario")]
        public string UsuarioId { get; set; } = null!;
        [Required]
        [JsonPropertyName("rol")]
        public string RolName { get; set; } = null!;
    }
}
