﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Postulantes.Data;
using Postulantes.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Postulantes.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RcEstadoCivilesController : ControllerBase
    {
        private readonly AppDbContext _context;

        public RcEstadoCivilesController(AppDbContext context)
        {
            _context = context;
        }

        // GET: api/RcEstadoCiviles
        [HttpGet]
        public async Task<ActionResult<IEnumerable<RcEstadoCivil>>> GetRcEstadoCiviles()
        {
            if (_context.RcEstadoCiviles == null)
            {
                return NotFound();
            }
            return await _context.RcEstadoCiviles.AsNoTracking().Where(x => x.EstadoCodigo == "APR").ToListAsync();
        }
    }
}
