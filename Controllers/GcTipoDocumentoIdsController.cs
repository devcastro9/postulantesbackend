﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Postulantes.Data;
using Postulantes.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Postulantes.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GcTipoDocumentoIdsController : ControllerBase
    {
        private readonly AppDbContext _context;

        public GcTipoDocumentoIdsController(AppDbContext context)
        {
            _context = context;
        }

        // GET: api/GcTipoDocumentoIds
        [HttpGet]
        public async Task<ActionResult<IEnumerable<GcTipoDocumentoId>>> GetGcTipoDocumentoIds()
        {
            if (_context.GcTipoDocumentoIds == null)
            {
                return NotFound();
            }
            return await _context.GcTipoDocumentoIds.AsNoTracking().Where(x => x.TipoDocIdentidadSin < 4).ToListAsync();
        }
    }
}
