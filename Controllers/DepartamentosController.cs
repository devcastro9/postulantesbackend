﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Postulantes.Data;
using Postulantes.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Postulantes.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DepartamentosController : ControllerBase
    {
        private readonly AppDbContext _context;
        private readonly ILogger<DepartamentosController> _logger;

        public DepartamentosController(AppDbContext context, ILogger<DepartamentosController> logger)
        {
            _context = context;
            _logger = logger;
        }

        // GET: api/Departamentos
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Departamento>>> GetDepartamentos()
        {
            if (_context.Departamentos == null)
            {
                return NotFound();
            }
            return await _context.Departamentos.AsNoTracking().Where(x => x.Habilitado == true).ToListAsync();
        }
    }
}
