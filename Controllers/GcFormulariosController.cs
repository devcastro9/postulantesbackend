﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Postulantes.Data;
using Postulantes.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Postulantes.Controllers
{
    //[Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class GcFormulariosController : ControllerBase
    {
        private readonly AppDbContext _context;
        private readonly ILogger<GcFormulariosController> _logger;

        public GcFormulariosController(AppDbContext context, ILogger<GcFormulariosController> logger)
        {
            _context = context;
            _logger = logger;
        }

        // GET: api/GcFormularios
        [HttpGet]
        public async Task<ActionResult<IEnumerable<GcFormulario>>> GetGcFormularios()
        {
            if (_context.GcFormularios == null)
            {
                return NotFound();
            }
            return await _context.GcFormularios.ToListAsync();
        }

        // GET: api/GcFormularios/5
        [HttpGet("{id}")]
        public async Task<ActionResult<GcFormulario>> GetGcFormulario(int id)
        {
            if (_context.GcFormularios == null)
            {
                return NotFound();
            }
            GcFormulario? gcFormulario = await _context.GcFormularios.FindAsync(id);
            if (gcFormulario == null)
            {
                return NotFound();
            }
            return gcFormulario;
        }

        // PUT: api/GcFormularios/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        //[HttpPut("{id}")]
        //public async Task<IActionResult> PutGcFormulario(int id, GcFormulario gcFormulario)
        //{
        //    if (id != gcFormulario.FormId)
        //    {
        //        return BadRequest();
        //    }

        //    _context.Entry(gcFormulario).State = EntityState.Modified;

        //    try
        //    {
        //        await _context.SaveChangesAsync();
        //    }
        //    catch (DbUpdateConcurrencyException)
        //    {
        //        if (!GcFormularioExists(id))
        //        {
        //            return NotFound();
        //        }
        //        else
        //        {
        //            throw;
        //        }
        //    }

        //    return NoContent();
        //}

        // POST: api/GcFormularios
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        //[HttpPost]
        //public async Task<ActionResult<GcFormulario>> PostGcFormulario(GcFormulario gcFormulario)
        //{
        //    if (_context.GcFormularios == null)
        //    {
        //        return Problem("Entity set 'AppDbContext.GcFormularios'  is null.");
        //    }
        //    _context.GcFormularios.Add(gcFormulario);
        //    await _context.SaveChangesAsync();

        //    return CreatedAtAction("GetGcFormulario", new { id = gcFormulario.FormId }, gcFormulario);
        //}

        // DELETE: api/GcFormularios/5
        //[HttpDelete("{id}")]
        //public async Task<IActionResult> DeleteGcFormulario(int id)
        //{
        //    if (_context.GcFormularios == null)
        //    {
        //        return NotFound();
        //    }
        //    GcFormulario? gcFormulario = await _context.GcFormularios.FindAsync(id);
        //    if (gcFormulario == null)
        //    {
        //        return NotFound();
        //    }

        //    _context.GcFormularios.Remove(gcFormulario);
        //    await _context.SaveChangesAsync();

        //    return NoContent();
        //}

        //private bool GcFormularioExists(int id)
        //{
        //    return (_context.GcFormularios?.Any(e => e.FormId == id)).GetValueOrDefault();
        //}
    }
}
