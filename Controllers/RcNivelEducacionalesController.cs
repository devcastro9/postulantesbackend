﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Postulantes.Data;
using Postulantes.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Postulantes.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RcNivelEducacionalesController : ControllerBase
    {
        private readonly AppDbContext _context;

        public RcNivelEducacionalesController(AppDbContext context)
        {
            _context = context;
        }

        // GET: api/RcNivelEducacionales
        [HttpGet]
        public async Task<ActionResult<IEnumerable<RcNivelEducacional>>> GetRcNivelEducacionales()
        {
            if (_context.RcNivelEducacionales == null)
            {
                return NotFound();
            }
            return await _context.RcNivelEducacionales.AsNoTracking().Where(x => x.Tipo == 3).ToListAsync();
        }
    }
}
