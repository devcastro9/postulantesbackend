﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Postulantes.Data;
using Postulantes.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Postulantes.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class GcRolesController : ControllerBase
    {
        private readonly AppDbContext _context;
        private readonly ILogger<GcRolesController> _logger;
        public GcRolesController(AppDbContext context, ILogger<GcRolesController> logger)
        {
            _context = context;
            _logger = logger;
        }

        // GET: api/GcRoles
        [HttpGet]
        public async Task<ActionResult<IEnumerable<GcRoles>>> GetGcRoles()
        {
            if (_context.GcRoles == null)
            {
                return NotFound();
            }
            return await _context.GcRoles.ToListAsync();
        }

        // GET: api/GcRoles/5
        [HttpGet("{id}")]
        public async Task<ActionResult<GcRoles>> GetGcRoles(int id)
        {
            if (_context.GcRoles == null)
            {
                return NotFound();
            }
            GcRoles? gcRoles = await _context.GcRoles.FindAsync(id);

            if (gcRoles == null)
            {
                return NotFound();
            }

            return gcRoles;
        }

        // PUT: api/GcRoles/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutGcRoles(int id, GcRoles gcRoles)
        {
            if (id != gcRoles.Id)
            {
                return BadRequest();
            }

            _context.Entry(gcRoles).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!GcRolesExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/GcRoles
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<GcRoles>> PostGcRoles(GcRoles gcRoles)
        {
            if (_context.GcRoles == null)
            {
                return Problem("Entity set 'AppDbContext.GcRoles'  is null.");
            }
            _context.GcRoles.Add(gcRoles);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetGcRoles", new { id = gcRoles.Id }, gcRoles);
        }

        // DELETE: api/GcRoles/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteGcRoles(int id)
        {
            if (_context.GcRoles == null)
            {
                return NotFound();
            }
            GcRoles? gcRoles = await _context.GcRoles.FindAsync(id);
            if (gcRoles == null)
            {
                return NotFound();
            }

            _context.GcRoles.Remove(gcRoles);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool GcRolesExists(int id)
        {
            return (_context.GcRoles?.Any(e => e.Id == id)).GetValueOrDefault();
        }
    }
}
