﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Postulantes.Data;
using Postulantes.Models;
using Postulantes.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Postulantes.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class BeneficiariosController : ControllerBase
    {
        private readonly AppDbContext _context;
        private readonly ILogger<BeneficiariosController> _logger;
        public BeneficiariosController(AppDbContext context, ILogger<BeneficiariosController> logger)
        {
            _context = context;
            _logger = logger;
        }

        // GET: api/Beneficiarios
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Beneficiario>>> GetBeneficiarios()
        {
            if (_context.Beneficiarios == null)
            {
                return NotFound();
            }
            return await _context.Beneficiarios.AsNoTracking().ToListAsync();
        }

        // GET: api/Beneficiarios
        [HttpGet("{id}")]
        public async Task<ActionResult<Beneficiario>> GetBeneficiario(int id)
        {
            if (_context.Beneficiarios == null)
            {
                return NotFound();
            }
            Beneficiario? beneficiario = await _context.Beneficiarios.FindAsync(id);
            if (beneficiario == null)
            {
                return NotFound();
            }
            beneficiario.EstudiosRealizados = await _context.EstudiosRealizados.AsNoTracking().Where(x => x.BeneficiarioId == id).ToListAsync();
            beneficiario.ExperienciaLaborales = await _context.ExperienciaLaborales.AsNoTracking().Where(m => m.BeneficiarioId == id).ToListAsync();
            beneficiario.Habilidades = await _context.Habilidades.AsNoTracking().Where(m => m.BeneficiarioId == id).ToListAsync();
            beneficiario.Idiomas = await _context.Idiomas.AsNoTracking().Where(m => m.BeneficiarioId == id).ToListAsync();
            return beneficiario;
        }

        // POST: api/Beneficiarios
        [AllowAnonymous]
        [HttpPost]
        public async Task<ActionResult<Respuesta>> PostBeneficiario(Beneficiario beneficiario)
        {
            if (_context.Beneficiarios == null)
            {
                return Problem("Entity is null.");
            }
            try
            {
                _context.Beneficiarios.Add(beneficiario);
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                return BadRequest(new Respuesta() { Error = false, Mensaje = ex.Message });
            }
            return CreatedAtAction(null, new Respuesta() { Error = false, Mensaje = "Postulación guardada." });
        }
    }
}
