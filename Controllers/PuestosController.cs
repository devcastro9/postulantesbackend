﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Postulantes.Data;
using Postulantes.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Postulantes.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PuestosController : ControllerBase
    {
        private readonly AppDbContext _context;

        public PuestosController(AppDbContext context)
        {
            _context = context;
        }

        // GET: api/Puestos
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Puesto>>> GetPuestos()
        {
            if (_context.Puestos == null)
            {
                return NotFound();
            }
            return await _context.Puestos.AsNoTracking().Include(x => x.Departamento).Where(x => x.EstadoRegistroId == 1 && x.Publicado == true).Select(c => new Puesto() { IdPuesto = c.IdPuesto, Descripcion = string.Concat(c.Descripcion, " - ", c.Departamento!.Descripcion) }).ToListAsync();
        }
    }
}
