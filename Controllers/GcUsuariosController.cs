﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Postulantes.Data;
using Postulantes.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Postulantes.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class GcUsuariosController : ControllerBase
    {
        private readonly AppDbContext _context;

        public GcUsuariosController(AppDbContext context)
        {
            _context = context;
        }

        // GET: api/GcUsuarios
        [HttpGet]
        public async Task<ActionResult<IEnumerable<GcUsuario>>> GetGcUsuarios()
        {
            if (_context.GcUsuarios == null)
            {
                return NotFound();
            }
            return await _context.GcUsuarios.ToListAsync();
        }

        // GET: api/GcUsuarios/5
        [HttpGet("{id}")]
        public async Task<ActionResult<GcUsuario>> GetGcUsuario(string id)
        {
            if (_context.GcUsuarios == null)
            {
                return NotFound();
            }
            GcUsuario? gcUsuario = await _context.GcUsuarios.FindAsync(id);

            if (gcUsuario == null)
            {
                return NotFound();
            }

            return gcUsuario;
        }

        // PUT: api/GcUsuarios/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutGcUsuario(string id, GcUsuario gcUsuario)
        {
            if (id != gcUsuario.UsrCodigo)
            {
                return BadRequest();
            }

            _context.Entry(gcUsuario).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!GcUsuarioExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/GcUsuarios
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<GcUsuario>> PostGcUsuario(GcUsuario gcUsuario)
        {
            if (_context.GcUsuarios == null)
            {
                return Problem("Entity set 'AppDbContext.GcUsuarios'  is null.");
            }
            _context.GcUsuarios.Add(gcUsuario);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (GcUsuarioExists(gcUsuario.UsrCodigo))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetGcUsuario", new { id = gcUsuario.UsrCodigo }, gcUsuario);
        }

        // DELETE: api/GcUsuarios/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteGcUsuario(string id)
        {
            if (_context.GcUsuarios == null)
            {
                return NotFound();
            }
            GcUsuario? gcUsuario = await _context.GcUsuarios.FindAsync(id);
            if (gcUsuario == null)
            {
                return NotFound();
            }

            _context.GcUsuarios.Remove(gcUsuario);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool GcUsuarioExists(string id)
        {
            return (_context.GcUsuarios?.Any(e => e.UsrCodigo == id)).GetValueOrDefault();
        }
    }
}
