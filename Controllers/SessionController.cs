﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Postulantes.Models;
using Postulantes.Services;
using Postulantes.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using SignInResult = Microsoft.AspNetCore.Identity.SignInResult;

namespace Postulantes.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SessionController : ControllerBase
    {
        private readonly UserManager<UsuarioNet> _userManager;
        private readonly SignInManager<UsuarioNet> _signInManager;
        private readonly IConfiguration _configuration;
        private readonly IHttpContextAccessor _contextAccessor;
        private readonly IJwtgenerador _jwtgenerador;
        private readonly ILogger<SessionController> _logger;

        public SessionController(UserManager<UsuarioNet> userManager, SignInManager<UsuarioNet> signInManager, IConfiguration configuration, IHttpContextAccessor httpContextAccessor, IJwtgenerador jwtgenerador, ILogger<SessionController> logger)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _configuration = configuration;
            _contextAccessor = httpContextAccessor;
            _jwtgenerador = jwtgenerador;
            _logger = logger;
        }

        [AllowAnonymous]
        [HttpPost("Login")]
        public async Task<ActionResult<LoginResponse>> Login(LoginModel model)
        {
            string TokenGenerado = "";
            DateTime expiracionToken;
            UsuarioNet? usuario = await _userManager.FindByNameAsync(model.UsuarioCodigo);
            if (usuario == null)
            {
                return NotFound(new LoginResponse() { IsLogin = false, Token = TokenGenerado, Expiracion = DateTime.Now, Mensaje = "No se encontro el usuario" });
            }
            bool puedeLogin = await _signInManager.CanSignInAsync(usuario);
            if (!puedeLogin)
            {
                return Unauthorized(new LoginResponse() { IsLogin = false, Token = TokenGenerado, Expiracion = DateTime.Now, Mensaje = "Su cuenta esta bloqueada" });
            }
            SignInResult result = await _signInManager.CheckPasswordSignInAsync(usuario, model.Password, false);
            if (result.Succeeded)
            {
                (TokenGenerado, expiracionToken) = await _jwtgenerador.CrearToken(usuario, _userManager, _configuration);
                _logger.LogInformation("Token generado");
                return Ok(new LoginResponse() { IsLogin = true, Token = TokenGenerado, Expiracion = expiracionToken, Mensaje = "Autenticacion exitosa" });
            }
            return Unauthorized(new LoginResponse() { IsLogin = false, Token = TokenGenerado, Expiracion = DateTime.Now, Mensaje = "No se puede autenticar" });
        }

        [Authorize]
        [HttpGet("Me")]
        public ActionResult<DataUserModel> GetData()
        {
            if (_contextAccessor.HttpContext == null)
            {
                return BadRequest();
            }
            IEnumerable<Claim> claimsToken = _contextAccessor.HttpContext.User.Claims;
            DataUserModel result = new()
            {
                Id = claimsToken.FirstOrDefault(x => x.Type == ClaimTypes.PrimarySid)?.Value,
                UserName = claimsToken.FirstOrDefault(x => x.Type == ClaimTypes.Sid)?.Value,
                Email = claimsToken.FirstOrDefault(x => x.Type == ClaimTypes.Email)?.Value,
                DepartamentoId = Convert.ToInt32(claimsToken.FirstOrDefault(x => x.Type == ClaimTypes.StateOrProvince)?.Value)
            };
            Parallel.ForEach(claimsToken.Where(x => x.Type == ClaimTypes.Role), rol => result.Roles.Add(rol.Value));
            return Ok(result);
        }
    }
}
