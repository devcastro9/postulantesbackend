﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Postulantes.Data;
using Postulantes.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Postulantes.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class GcControlesController : ControllerBase
    {
        private readonly AppDbContext _context;
        private readonly ILogger<GcControlesController> _logger;

        public GcControlesController(AppDbContext context, ILogger<GcControlesController> logger)
        {
            _context = context;
            _logger = logger;
        }

        // GET: api/GcControles
        [HttpGet]
        public async Task<ActionResult<IEnumerable<GcControles>>> GetGcControles()
        {
            if (_context.GcControles == null)
            {
                return NotFound();
            }
            return await _context.GcControles.ToListAsync();
        }

        // GET: api/GcControles/5
        [HttpGet("{id}")]
        public async Task<ActionResult<GcControles>> GetGcControles(int id)
        {
            if (_context.GcControles == null)
            {
                return NotFound();
            }
            GcControles? gcControles = await _context.GcControles.FindAsync(id);

            if (gcControles == null)
            {
                return NotFound();
            }

            return gcControles;
        }

        // GET: api/GcControles/Form/5
        [HttpGet("Form/{idForm}")]
        public async Task<ActionResult<IEnumerable<GcControles>>> GetGcControlesByForm(int idForm)
        {
            if (_context.GcControles == null)
            {
                return NotFound();
            }
            List<GcControles> gcControles = await _context.GcControles.Where(m => m.FormId == idForm).ToListAsync();

            if (gcControles.Count == 0)
            {
                return NotFound();
            }

            return gcControles;
        }

        //// PUT: api/GcControles/5
        //// To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        //[HttpPut("{id}")]
        //public async Task<IActionResult> PutGcControles(int id, GcControles gcControles)
        //{
        //    if (id != gcControles.CtrlId)
        //    {
        //        return BadRequest();
        //    }

        //    _context.Entry(gcControles).State = EntityState.Modified;

        //    try
        //    {
        //        await _context.SaveChangesAsync();
        //    }
        //    catch (DbUpdateConcurrencyException)
        //    {
        //        if (!GcControlesExists(id))
        //        {
        //            return NotFound();
        //        }
        //        else
        //        {
        //            throw;
        //        }
        //    }

        //    return NoContent();
        //}

        //// POST: api/GcControles
        //// To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        //[HttpPost]
        //public async Task<ActionResult<GcControles>> PostGcControles(GcControles gcControles)
        //{
        //  if (_context.GcControles == null)
        //  {
        //      return Problem("Entity set 'AppDbContext.GcControles'  is null.");
        //  }
        //    _context.GcControles.Add(gcControles);
        //    await _context.SaveChangesAsync();

        //    return CreatedAtAction("GetGcControles", new { id = gcControles.CtrlId }, gcControles);
        //}

        //// DELETE: api/GcControles/5
        //[HttpDelete("{id}")]
        //public async Task<IActionResult> DeleteGcControles(int id)
        //{
        //    if (_context.GcControles == null)
        //    {
        //        return NotFound();
        //    }
        //    var gcControles = await _context.GcControles.FindAsync(id);
        //    if (gcControles == null)
        //    {
        //        return NotFound();
        //    }

        //    _context.GcControles.Remove(gcControles);
        //    await _context.SaveChangesAsync();

        //    return NoContent();
        //}

        //private bool GcControlesExists(int id)
        //{
        //    return (_context.GcControles?.Any(e => e.CtrlId == id)).GetValueOrDefault();
        //}
    }
}
