﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Postulantes.Controllers
{
    [Authorize(Roles = "Administrador")]
    [Route("api/[controller]")]
    [ApiController]
    public class RolesNetController : ControllerBase
    {
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly ILogger<RolesNetController> _logger;
        public RolesNetController(RoleManager<IdentityRole> role, ILogger<RolesNetController> logger)
        {
            _roleManager = role;
            _logger = logger;
        }

        [HttpGet("Roles")]
        public async Task<ActionResult<IEnumerable<IdentityRole>>> GetRoles()
        {
            return await _roleManager.Roles.AsNoTracking().ToListAsync();
        }

        [HttpGet("Roles/{id}")]
        public async Task<ActionResult<IdentityRole>> GetRol(string id)
        {
            IdentityRole? result = await _roleManager.FindByIdAsync(id);
            if (result == null)
            {
                return NotFound();
            }
            return result;
        }

        [HttpPut("Roles/{id}")]
        public async Task<IActionResult> ModificarRol(string id, IdentityRole role)
        {
            if (id != role.Id)
            {
                return BadRequest();
            }
            await _roleManager.SetRoleNameAsync(role, role.Name);
            return NoContent();
        }

        [HttpPost("Roles")]
        public async Task<ActionResult<IdentityRole>> CrearRol(string rolName)
        {
            IdentityRole rol = new()
            {
                Name = rolName
            };
            IdentityResult result = await _roleManager.CreateAsync(rol);
            if (!result.Succeeded)
            {
                return BadRequest();
            }
            _logger.LogInformation(rol.Name);
            return CreatedAtAction("GetRol", new { id = rol.Id }, rol);
        }

        [HttpDelete("Roles/{id}")]
        public async Task<IActionResult> EliminarRol(string id)
        {
            IdentityRole? rol = await _roleManager.FindByIdAsync(id);
            if (rol == null)
            {
                return NotFound();
            }
            await _roleManager.DeleteAsync(rol);
            return NoContent();
        }
    }
}
