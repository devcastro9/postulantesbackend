﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Postulantes.Data;
using Postulantes.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Postulantes.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GcPaisesController : ControllerBase
    {
        private readonly AppDbContext _context;
        private readonly ILogger<GcPaisesController> _logger;

        public GcPaisesController(AppDbContext context, ILogger<GcPaisesController> logger)
        {
            _context = context;
            _logger = logger;
        }

        // GET: api/GcPaises/filter
        [HttpGet("{filter}")]
        public async Task<ActionResult<IEnumerable<GcPais>>> GetGcPaises(string filter)
        {
            if (_context.GcPaises == null)
            {
                return NotFound();
            }
            IQueryable<GcPais> query = _context.GcPaises.AsNoTracking().Where(x => x.Id > 0);
            if (filter.Length < 2)
            {
                return NoContent();
            }
            query = query.Where(x => EF.Functions.Like(x.PaisDescripcion!, $"%{filter}%"));
            return await query.ToListAsync();
        }
    }
}
