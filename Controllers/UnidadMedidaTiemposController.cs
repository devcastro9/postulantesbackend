﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Postulantes.Data;
using Postulantes.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Postulantes.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UnidadMedidaTiemposController : ControllerBase
    {
        private readonly AppDbContext _context;

        public UnidadMedidaTiemposController(AppDbContext context)
        {
            _context = context;
        }

        // GET: api/UnidadMedidaTiempos
        [HttpGet]
        public async Task<ActionResult<IEnumerable<UnidadMedidaTiempo>>> GetUnidadMedidaTiempos()
        {
            if (_context.UnidadMedidaTiempos == null)
            {
                return NotFound();
            }
            return await _context.UnidadMedidaTiempos.AsNoTracking().Where(x => x.EstadoRegistroId == 1).ToListAsync();
        }
    }
}
