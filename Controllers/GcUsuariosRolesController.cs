﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Postulantes.Data;
using Postulantes.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Postulantes.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class GcUsuariosRolesController : ControllerBase
    {
        private readonly AppDbContext _context;
        private readonly ILogger<GcUsuariosRolesController> _logger;

        public GcUsuariosRolesController(AppDbContext context, ILogger<GcUsuariosRolesController> logger)
        {
            _context = context;
            _logger = logger;
        }

        // GET: api/GcUsuariosRoles
        [HttpGet]
        public async Task<ActionResult<IEnumerable<GcUsuariosRoles>>> GetGcUsuariosRoles()
        {
            if (_context.GcUsuariosRoles == null)
            {
                return NotFound();
            }
            return await _context.GcUsuariosRoles.ToListAsync();
        }

        // GET: api/GcUsuariosRoles/5
        [HttpGet("{id}")]
        public async Task<ActionResult<GcUsuariosRoles>> GetGcUsuariosRoles(string id)
        {
            if (_context.GcUsuariosRoles == null)
            {
                return NotFound();
            }
            GcUsuariosRoles? gcUsuariosRoles = await _context.GcUsuariosRoles.FindAsync(id);

            if (gcUsuariosRoles == null)
            {
                return NotFound();
            }

            return gcUsuariosRoles;
        }

        // PUT: api/GcUsuariosRoles/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutGcUsuariosRoles(string id, GcUsuariosRoles gcUsuariosRoles)
        {
            if (id != gcUsuariosRoles.UsrCodigoId)
            {
                return BadRequest();
            }

            _context.Entry(gcUsuariosRoles).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!GcUsuariosRolesExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/GcUsuariosRoles
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<GcUsuariosRoles>> PostGcUsuariosRoles(GcUsuariosRoles gcUsuariosRoles)
        {
            if (_context.GcUsuariosRoles == null)
            {
                return Problem("Entity set 'AppDbContext.GcUsuariosRoles'  is null.");
            }
            _context.GcUsuariosRoles.Add(gcUsuariosRoles);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (GcUsuariosRolesExists(gcUsuariosRoles.UsrCodigoId))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetGcUsuariosRoles", new { id = gcUsuariosRoles.UsrCodigoId }, gcUsuariosRoles);
        }

        // DELETE: api/GcUsuariosRoles/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteGcUsuariosRoles(string id)
        {
            if (_context.GcUsuariosRoles == null)
            {
                return NotFound();
            }
            GcUsuariosRoles? gcUsuariosRoles = await _context.GcUsuariosRoles.FindAsync(id);
            if (gcUsuariosRoles == null)
            {
                return NotFound();
            }

            _context.GcUsuariosRoles.Remove(gcUsuariosRoles);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool GcUsuariosRolesExists(string id)
        {
            return (_context.GcUsuariosRoles?.Any(e => e.UsrCodigoId == id)).GetValueOrDefault();
        }
    }
}
