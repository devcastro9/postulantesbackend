﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Postulantes.Services;
using Postulantes.ViewModels;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Postulantes.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FotosController : ControllerBase
    {
        private readonly IFtpServer _ftpServer;
        private readonly ILogger<FotosController> _logger;
        private readonly string[] PermittedExtensions = { ".png", ".jpg", ".jpeg" };
        public FotosController(IFtpServer ftpServer, ILogger<FotosController> logger)
        {
            _ftpServer = ftpServer;
            _logger = logger;
        }

        [HttpPost]
        public async Task<ActionResult<ResponseFile>> CargarPost(IFormFile file)
        {
            bool EstadoArchivo = false;
            _logger.LogInformation("Carga de foto iniciada", DateTime.UtcNow.ToLongTimeString());
            if (file.Length == 0)
            {
                return Problem("Tamaño no valido");
            }
            string ext = Path.GetExtension(file.FileName).ToLowerInvariant();
            if (string.IsNullOrEmpty(ext) || !PermittedExtensions.Contains(ext))
            {
                return Problem("Formato no permitido");
            }
            try
            {
                // Carga desde Memoria RAM a FTP
                string ruta = $"{DateTime.Now:yyyyMMdd_HHmmss}_{file.FileName}";
                _logger.LogInformation("Carga de archivo");
                using (MemoryStream ArchivoMemoria = new())
                {
                    await file.CopyToAsync(ArchivoMemoria);
                    byte[] data = ArchivoMemoria.ToArray();
                    EstadoArchivo = _ftpServer.Cargar(data, ruta);
                }
                // Carga en fisico
                //string upload = Path.Combine(_enviroment.ContentRootPath, "uploads");
                //string filepath = Path.Combine(upload, file.FileName);
                //_logger.LogInformation("Carga de archivo");
                //using (FileStream fileStream = System.IO.File.Create(filepath))
                //{
                //    await file.CopyToAsync(fileStream);
                //}
                _logger.LogInformation("Carga de foto finalizada");
                return EstadoArchivo ? Ok(new ResponseFile() { Estado = EstadoArchivo, RutaArchivo = ruta }) : BadRequest(new ResponseFile() { Estado = EstadoArchivo, RutaArchivo = ruta });
            }
            catch (Exception ex)
            {
                return BadRequest(new ResponseFile() { Estado = false, RutaArchivo = ex.Message });
            }
        }
    }
}
