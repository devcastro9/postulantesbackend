﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Postulantes.Data;
using Postulantes.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Postulantes.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class GcRightsController : ControllerBase
    {
        private readonly AppDbContext _context;
        private readonly ILogger<GcRightsController> _logger;

        public GcRightsController(AppDbContext context, ILogger<GcRightsController> logger)
        {
            _context = context;
            _logger = logger;
        }

        // GET: api/GcRights
        [HttpGet]
        public async Task<ActionResult<IEnumerable<GcRight>>> GetGcRights()
        {
            if (_context.GcRights == null)
            {
                return NotFound();
            }
            return await _context.GcRights.ToListAsync();
        }

        // GET: api/GcRights/5
        [HttpGet("{id}")]
        public async Task<ActionResult<GcRight>> GetGcRight(int id)
        {
            if (_context.GcRights == null)
            {
                return NotFound();
            }
            GcRight? gcRight = await _context.GcRights.FindAsync(id);

            if (gcRight == null)
            {
                return NotFound();
            }

            return gcRight;
        }

        // PUT: api/GcRights/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutGcRight(int id, GcRight gcRight)
        {
            if (id != gcRight.RightId)
            {
                return BadRequest();
            }

            _context.Entry(gcRight).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!GcRightExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/GcRights
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<GcRight>> PostGcRight(GcRight gcRight)
        {
            if (_context.GcRights == null)
            {
                return Problem("Entity set 'AppDbContext.GcRights'  is null.");
            }
            _context.GcRights.Add(gcRight);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetGcRight", new { id = gcRight.RightId }, gcRight);
        }

        // DELETE: api/GcRights/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteGcRight(int id)
        {
            if (_context.GcRights == null)
            {
                return NotFound();
            }
            GcRight? gcRight = await _context.GcRights.FindAsync(id);
            if (gcRight == null)
            {
                return NotFound();
            }

            _context.GcRights.Remove(gcRight);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool GcRightExists(int id)
        {
            return (_context.GcRights?.Any(e => e.RightId == id)).GetValueOrDefault();
        }
    }
}
