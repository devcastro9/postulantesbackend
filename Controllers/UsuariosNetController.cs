﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Postulantes.Models;
using Postulantes.Services;
using Postulantes.ViewModels;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Postulantes.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class UsuariosNetController : ControllerBase
    {
        private readonly UserManager<UsuarioNet> _userManager;
        private readonly SignInManager<UsuarioNet> _signInManager;
        private readonly IConfiguration _configuration;
        private readonly IHttpContextAccessor _contextAccessor;
        private readonly IJwtgenerador _jwtgenerador;
        private readonly ILogger<UsuariosNetController> _logger;
        public UsuariosNetController(UserManager<UsuarioNet> userManager, SignInManager<UsuarioNet> signInManager, IConfiguration configuration, IHttpContextAccessor httpContextAccessor, IJwtgenerador jwtgenerador, ILogger<UsuariosNetController> logger)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _configuration = configuration;
            _contextAccessor = httpContextAccessor;
            _jwtgenerador = jwtgenerador;
            _logger = logger;
        }

        //[AllowAnonymous]
        //[HttpPost("Login")]
        //public async Task<ActionResult<LoginResponse>> Login(LoginModel model)
        //{
        //    UsuarioNet? usuario = await _userManager.FindByNameAsync(model.UsuarioCodigo);
        //    if (usuario == null)
        //    {
        //        return NotFound(new LoginResponse() { IsLogin = false, Token = "", Mensaje = "No se encontro el usuario" });
        //    }
        //    bool puedeLogin = await _signInManager.CanSignInAsync(usuario);
        //    if (!puedeLogin)
        //    {
        //        return Unauthorized(new LoginResponse() { IsLogin = false, Token = "", Mensaje = "Su cuenta esta bloqueada" });
        //    }
        //    SignInResult result = await _signInManager.CheckPasswordSignInAsync(usuario, model.Password, false);
        //    if (result.Succeeded)
        //    {
        //        string TokenGenerado = await _jwtgenerador.CrearToken(usuario, _userManager, _configuration);
        //        return Ok(new LoginResponse() { IsLogin = true, Token = TokenGenerado, Mensaje = "Autenticacion exitosa" });
        //    }
        //    return Unauthorized(new LoginResponse() { IsLogin = false, Token = "", Mensaje = "No se puede autenticar" });
        //}

        [HttpGet("Usuarios")]
        public async Task<ActionResult<IEnumerable<UsuarioNet>>> GetUsuarios()
        {
            return await _userManager.Users.AsNoTracking().ToListAsync();
        }

        [HttpGet("Usuario/{id}")]
        public async Task<ActionResult<UsuarioNet>> GetUsuario(string id)
        {
            UsuarioNet? result = await _userManager.FindByIdAsync(id);
            if (result == null)
            {
                return NotFound();
            }
            return result;
        }

        [HttpGet("Me")]
        public async Task<ActionResult<string>> GetData()
        {
            await Task.Delay(100);
            if (_contextAccessor.HttpContext == null)
            {
                return BadRequest();
            }
            string? userName = _contextAccessor.HttpContext.User.Claims.FirstOrDefault(x => x.Type == ClaimTypes.Sid)?.Value;
            if (userName == null)
            {
                return NotFound();
            }
            return Ok(userName);
        }

        [HttpPut("Password")]
        public async Task<ActionResult> CambiarPassword(UsuarioNet usuario, string oldpass, string newpass)
        {
            _logger.LogInformation("Cambio de password");
            IdentityResult result = await _userManager.ChangePasswordAsync(usuario, oldpass, newpass);
            return result.Succeeded ? NoContent() : BadRequest();
        }

        [HttpPut("AddRol")]
        public async Task<ActionResult> AddRoles(AddRolModel model)
        {
            UsuarioNet? usuario = await _userManager.FindByIdAsync(model.UsuarioId);
            if (usuario == null)
            {
                return NotFound();
            }
            IdentityResult result = await _userManager.AddToRoleAsync(usuario, model.RolName);
            if (!result.Succeeded)
            {
                return BadRequest();
            }
            return NoContent();
        }

        [HttpPost("Usuario")]
        public async Task<ActionResult<UsuarioNet>> CrearUsuario(UsuarioNet usuario)
        {
            IdentityResult result = await _userManager.CreateAsync(usuario, _configuration["PassDefault"] ?? "User123*");
            if (!result.Succeeded)
            {
                return BadRequest();
            }
            return CreatedAtAction("GetUsuario", new { id = usuario.Id }, usuario);
        }

        [HttpDelete("Usuario/{id}")]
        public async Task<IActionResult> EliminarUsuario(string id)
        {
            UsuarioNet? usuario = await _userManager.FindByIdAsync(id);
            if (usuario == null)
            {
                return NotFound();
            }
            await _userManager.DeleteAsync(usuario);
            return NoContent();
        }
    }
}
