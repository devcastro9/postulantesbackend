#!/bin/bash
# Borrado de contenedores
sudo docker kill $(sudo docker ps -q)
sudo docker rm $(sudo docker ps -a -q)
sudo docker rmi $(sudo docker images -q)
# Borrado total (usese con precaucion)
sudo docker system prune -a -f
# Despliegue de postulantes backend
cd /home/developer/Documentos/postulantesbackend
git pull
sudo docker build -t postulantes-backend .
sudo docker run --name postulantes -d --rm -p 5032:5032 postulantes-backend

# Despliegue de facturacion
cd /home/developer/Documentos/sofiabackend
git pull
sudo docker build -t facturacion-backend .
sudo docker run --name facturacion -d --rm -p 5000:5000 facturacion-backend
