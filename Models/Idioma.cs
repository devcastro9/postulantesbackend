﻿using Microsoft.EntityFrameworkCore;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Postulantes.Models;

[Table("Idioma", Schema = "rrhh")]
public partial class Idioma
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int IdIdioma { get; set; }

    public int? BeneficiarioId { get; set; }

    [StringLength(100)]
    [Unicode(false)]
    public string? IdiomaDescripcion { get; set; }

    [StringLength(150)]
    [Unicode(false)]
    public string? NivelEscrito { get; set; }

    [StringLength(150)]
    [Unicode(false)]
    public string? NivelOral { get; set; }

    [StringLength(150)]
    [Unicode(false)]
    public string? NivelLectura { get; set; }

    public int? EstadoRegistroId { get; set; }

    [Column(TypeName = "datetime")]
    public DateTime? FechaRegistro { get; set; }

    [ForeignKey("BeneficiarioId")]
    [InverseProperty("Idiomas")]
    public virtual Beneficiario? Beneficiario { get; set; }
}
