﻿using Microsoft.EntityFrameworkCore;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Postulantes.Models;

[PrimaryKey("UsrCodigoId", "RoleId")]
[Table("gc_usuarios_roles")]
public partial class GcUsuariosRoles
{
    [Key]
    [Column("usr_codigo_id")]
    [StringLength(20)]
    [Unicode(false)]
    public string UsrCodigoId { get; set; } = null!;

    [Key]
    public int RoleId { get; set; }

    [Column("fecha", TypeName = "datetime")]
    public DateTime? Fecha { get; set; }

    [ForeignKey("RoleId")]
    [InverseProperty("GcUsuariosRoles")]
    public virtual GcRoles Role { get; set; } = null!;

    [ForeignKey("UsrCodigoId")]
    [InverseProperty("GcUsuariosRoles")]
    public virtual GcUsuario UsrCodigo { get; set; } = null!;
}
