﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace Postulantes.Models;

[Table("rc_estado_civil")]
public partial class RcEstadoCivil
{
    [Key]
    [Column("estado_civil_codigo")]
    [StringLength(1)]
    [Unicode(false)]
    public string EstadoCivilCodigo { get; set; } = null!;

    [Column("estado_civil_descripcion")]
    [StringLength(30)]
    [Unicode(false)]
    public string? EstadoCivilDescripcion { get; set; }

    [JsonIgnore]
    [Column("estado_codigo")]
    [StringLength(3)]
    [Unicode(false)]
    public string? EstadoCodigo { get; set; }

    [JsonIgnore]
    [Column("fecha_registro", TypeName = "datetime")]
    public DateTime? FechaRegistro { get; set; }

    [JsonIgnore]
    [Column("hora_registro")]
    [StringLength(8)]
    [Unicode(false)]
    public string? HoraRegistro { get; set; }

    [JsonIgnore]
    [Column("usr_codigo")]
    [StringLength(20)]
    [Unicode(false)]
    public string? UsrCodigo { get; set; }

    [JsonIgnore]
    [InverseProperty("EstadoCivil")]
    public virtual ICollection<Beneficiario> Beneficiarios { get; } = new List<Beneficiario>();
}
