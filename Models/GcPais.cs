﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace Postulantes.Models;

[Table("gc_pais")]
public partial class GcPais
{
    [Key]
    [Column("id")]
    public int Id { get; set; }

    [JsonIgnore]
    [Column("pais_codigo")]
    [StringLength(3)]
    [Unicode(false)]
    public string PaisCodigo { get; set; } = null!;

    [Column("pais_descripcion")]
    [StringLength(100)]
    [Unicode(false)]
    public string? PaisDescripcion { get; set; }

    [JsonIgnore]
    [Column("pais_sigla")]
    [StringLength(10)]
    [Unicode(false)]
    public string? PaisSigla { get; set; }

    [JsonIgnore]
    [Column("pais_cod_telefonico")]
    [StringLength(10)]
    [Unicode(false)]
    public string? PaisCodTelefonico { get; set; }

    [JsonIgnore]
    [Column("pais_descripcion_ingles")]
    [StringLength(100)]
    [Unicode(false)]
    public string? PaisDescripcionIngles { get; set; }

    [JsonIgnore]
    [Column("pais_cod_iso_num")]
    [StringLength(10)]
    [Unicode(false)]
    public string? PaisCodIsoNum { get; set; }

    [JsonIgnore]
    [Column("pais_cod_aeropuerto")]
    [StringLength(10)]
    [Unicode(false)]
    public string? PaisCodAeropuerto { get; set; }

    [JsonIgnore]
    [Column("pais_cod_horario")]
    [StringLength(10)]
    [Unicode(false)]
    public string? PaisCodHorario { get; set; }

    [JsonIgnore]
    [Column("pais_continente")]
    [StringLength(25)]
    [Unicode(false)]
    public string? PaisContinente { get; set; }

    [JsonIgnore]
    [Column("estado_codigo")]
    [StringLength(3)]
    [Unicode(false)]
    public string? EstadoCodigo { get; set; }

    [JsonIgnore]
    [Column("fecha_registro", TypeName = "datetime")]
    public DateTime? FechaRegistro { get; set; }

    [JsonIgnore]
    [Column("usr_codigo")]
    [StringLength(20)]
    [Unicode(false)]
    public string? UsrCodigo { get; set; }

    [JsonIgnore]
    [Column("hora_registro")]
    [StringLength(8)]
    [Unicode(false)]
    public string? HoraRegistro { get; set; }

    [JsonIgnore]
    [InverseProperty("Pais")]
    public virtual ICollection<Beneficiario> Beneficiarios { get; } = new List<Beneficiario>();

    [JsonIgnore]
    [InverseProperty("Pais")]
    public virtual ICollection<EstudiosRealizado> EstudiosRealizados { get; } = new List<EstudiosRealizado>();

    [JsonIgnore]
    [InverseProperty("Pais")]
    public virtual ICollection<ExperienciaLaboral> ExperienciaLaborales { get; } = new List<ExperienciaLaboral>();
}
