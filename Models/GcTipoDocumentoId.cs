﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace Postulantes.Models;

[Table("gc_tipo_documento_id")]
public partial class GcTipoDocumentoId
{
    [Key]
    [Column("tipodoc_codigo")]
    [StringLength(3)]
    [Unicode(false)]
    public string TipodocCodigo { get; set; } = null!;

    [Column("tipodoc_descripcion")]
    [StringLength(50)]
    [Unicode(false)]
    public string? TipodocDescripcion { get; set; }

    [JsonIgnore]
    [Column("tipodoc_tipo")]
    [StringLength(1)]
    [Unicode(false)]
    public string? TipodocTipo { get; set; }

    [JsonIgnore]
    [Column("TipoDocIdentidadSIN")]
    public int? TipoDocIdentidadSin { get; set; }

    [JsonIgnore]
    [Column("DocIdentidadSIN")]
    [StringLength(3)]
    [Unicode(false)]
    public string? DocIdentidadSin { get; set; }

    [JsonIgnore]
    [Column("estado_codigo")]
    [StringLength(3)]
    [Unicode(false)]
    public string? EstadoCodigo { get; set; }

    [JsonIgnore]
    [Column("usr_codigo")]
    [StringLength(20)]
    [Unicode(false)]
    public string? UsrCodigo { get; set; }

    [JsonIgnore]
    [Column("fecha_registro", TypeName = "datetime")]
    public DateTime? FechaRegistro { get; set; }

    [JsonIgnore]
    [Column("hora_registro")]
    [StringLength(8)]
    [Unicode(false)]
    public string? HoraRegistro { get; set; }

    [JsonIgnore]
    [Column("tipodoc_num")]
    public int? TipodocNum { get; set; }

    [JsonIgnore]
    [InverseProperty("DocumentoRespaldo")]
    public virtual ICollection<Beneficiario> Beneficiarios { get; } = new List<Beneficiario>();
}
