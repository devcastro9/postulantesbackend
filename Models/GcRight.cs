﻿using Microsoft.EntityFrameworkCore;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Postulantes.Models;

[Table("gc_right")]
public partial class GcRight
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int RightId { get; set; }

    public int? IdRole { get; set; }

    public int? FormId { get; set; }

    public int? CtrlId { get; set; }

    public bool? Visible { get; set; }

    public bool? Enabled { get; set; }

    public bool? Locked { get; set; }

    [Column("usuario_modificado")]
    [StringLength(20)]
    [Unicode(false)]
    public string UsuarioModificado { get; set; } = null!;

    [Column("fecha_registro", TypeName = "datetime")]
    public DateTime? FechaRegistro { get; set; }

    [ForeignKey("CtrlId")]
    [InverseProperty("GcRights")]
    public virtual GcControles? Ctrl { get; set; }

    [ForeignKey("IdRole")]
    [InverseProperty("GcRights")]
    public virtual GcRoles? IdRoleNavigation { get; set; }
}
