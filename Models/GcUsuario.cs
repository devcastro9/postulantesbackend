﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Postulantes.Models;

[Table("gc_usuarios")]
public partial class GcUsuario
{
    [Key]
    [Column("usr_codigo")]
    [StringLength(20)]
    [Unicode(false)]
    public string UsrCodigo { get; set; } = null!;

    [Column("beneficiario_codigo")]
    [StringLength(15)]
    [Unicode(false)]
    public string? BeneficiarioCodigo { get; set; }

    [Column("usr_nombres")]
    [StringLength(30)]
    [Unicode(false)]
    public string? UsrNombres { get; set; }

    [Column("usr_primer_apellido")]
    [StringLength(30)]
    [Unicode(false)]
    public string? UsrPrimerApellido { get; set; }

    [Column("usr_segundo_apellido")]
    [StringLength(30)]
    [Unicode(false)]
    public string? UsrSegundoApellido { get; set; }

    [Column("usr_clave")]
    [StringLength(15)]
    [Unicode(false)]
    public string? UsrClave { get; set; }

    [Column("dgral_codigo")]
    [StringLength(5)]
    [Unicode(false)]
    public string? DgralCodigo { get; set; }

    [Column("da_codigo")]
    [StringLength(7)]
    [Unicode(false)]
    public string? DaCodigo { get; set; }

    [Column("unidad_codigo")]
    [StringLength(15)]
    [Unicode(false)]
    public string? UnidadCodigo { get; set; }

    [Column("ocup_codigo")]
    public int? OcupCodigo { get; set; }

    [Column("usr_observaciones")]
    [StringLength(60)]
    [Unicode(false)]
    public string? UsrObservaciones { get; set; }

    [Column("idnivelacceso")]
    public int? Idnivelacceso { get; set; }

    [Column("estado_codigo")]
    [StringLength(3)]
    [Unicode(false)]
    public string? EstadoCodigo { get; set; }

    [Column("fecha_registro", TypeName = "datetime")]
    public DateTime? FechaRegistro { get; set; }

    [Column("hora_registro")]
    [StringLength(8)]
    [Unicode(false)]
    public string? HoraRegistro { get; set; }

    [Column("depto_codigo")]
    [StringLength(2)]
    [Unicode(false)]
    public string? DeptoCodigo { get; set; }

    [Column("validar_version")]
    public bool? ValidarVersion { get; set; }

    [InverseProperty("UsrCodigo")]
    public virtual ICollection<GcUsuariosRoles> GcUsuariosRoles { get; } = new List<GcUsuariosRoles>();
}
