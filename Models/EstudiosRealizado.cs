﻿using Microsoft.EntityFrameworkCore;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Postulantes.Models;

[Table("EstudiosRealizados", Schema = "rrhh")]
public partial class EstudiosRealizado
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int IdEstudiosRealizados { get; set; }

    public int? BeneficiarioId { get; set; }

    [Column("CarreraOCurso")]
    [StringLength(80)]
    [Unicode(false)]
    public string? CarreraOcurso { get; set; }

    [StringLength(80)]
    [Unicode(false)]
    public string? CentroEducativo { get; set; }

    [StringLength(80)]
    [Unicode(false)]
    public string? TituloObtenido { get; set; }

    public int? NivelEducativoId { get; set; }

    public int? Tipo { get; set; }

    [StringLength(50)]
    [Unicode(false)]
    public string? Ciudad { get; set; }

    public int? PaisId { get; set; }

    [Column(TypeName = "date")]
    public DateTime? FechaInicio { get; set; }

    [Column(TypeName = "date")]
    public DateTime? FechaFinalizacion { get; set; }

    public bool? EstudiandoActualmente { get; set; }

    public int? EstadoRegistroId { get; set; }

    [Column(TypeName = "datetime")]
    public DateTime? FechaRegistro { get; set; }

    [ForeignKey("BeneficiarioId")]
    [InverseProperty("EstudiosRealizados")]
    public virtual Beneficiario? Beneficiario { get; set; }

    [ForeignKey("NivelEducativoId")]
    [InverseProperty("EstudiosRealizados")]
    public virtual RcNivelEducacional? NivelEducativo { get; set; }

    [ForeignKey("PaisId")]
    [InverseProperty("EstudiosRealizados")]
    public virtual GcPais? Pais { get; set; }
}
