﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Postulantes.Models;

[Table("Beneficiario", Schema = "rrhh")]
public partial class Beneficiario
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int IdBeneficiario { get; set; }

    public int? BeneficiarioClasificacionId { get; set; }

    public int? PuestoId { get; set; }

    [StringLength(20)]
    [Unicode(false)]
    public string? DocumentoIdentidad { get; set; }

    [StringLength(3)]
    [Unicode(false)]
    public string? DocumentoRespaldoId { get; set; }

    public int? DepartamentoId { get; set; }

    [StringLength(4)]
    [Unicode(false)]
    public string? Iniciales { get; set; }

    [StringLength(35)]
    [Unicode(false)]
    public string? PrimerApellido { get; set; }

    [StringLength(35)]
    [Unicode(false)]
    public string? SegundoApellido { get; set; }

    [StringLength(30)]
    [Unicode(false)]
    public string? ApellidoCasada { get; set; }

    [StringLength(50)]
    [Unicode(false)]
    public string? Nombres { get; set; }

    [StringLength(20)]
    [Unicode(false)]
    public string? Nit { get; set; }

    [StringLength(100)]
    [Unicode(false)]
    public string? Denominacion { get; set; }

    [Column(TypeName = "date")]
    public DateTime? FechaNacimiento { get; set; }

    public int? PaisId { get; set; }

    public int? GeneroId { get; set; }

    [StringLength(1)]
    [Unicode(false)]
    public string? EstadoCivilId { get; set; }

    [StringLength(30)]
    [Unicode(false)]
    public string? TelefonoFijo { get; set; }

    [StringLength(30)]
    [Unicode(false)]
    public string? TelefonoOficina { get; set; }

    [StringLength(30)]
    [Unicode(false)]
    public string? TelefonoCelular { get; set; }

    [EmailAddress]
    [StringLength(50)]
    [Unicode(false)]
    public string? EmailPersonal { get; set; }

    [StringLength(50)]
    [Unicode(false)]
    public string? EmailOficina { get; set; }

    [StringLength(250)]
    [Unicode(false)]
    public string? DomicilioLegal { get; set; }

    public bool? LicenciaConducir { get; set; }

    public bool? LicenciaMotocicleta { get; set; }

    //[JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingDefault)]
    [StringLength(150)]
    [Unicode(false)]
    public string? FotoRuta { get; set; }

    public int? PretensionSalarial { get; set; }

    public int? EstadoRegistroId { get; set; }

    public int? UsuarioId { get; set; }

    [Column(TypeName = "datetime")]
    public DateTime? FechaRegistro { get; set; }

    public bool? Habilitado { get; set; }

    [ForeignKey("DepartamentoId")]
    [InverseProperty("Beneficiarios")]
    public virtual Departamento? Departamento { get; set; }

    [ForeignKey("DocumentoRespaldoId")]
    [InverseProperty("Beneficiarios")]
    public virtual GcTipoDocumentoId? DocumentoRespaldo { get; set; }

    [ForeignKey("EstadoCivilId")]
    [InverseProperty("Beneficiarios")]
    public virtual RcEstadoCivil? EstadoCivil { get; set; }

    [ForeignKey("EstadoRegistroId")]
    [InverseProperty("Beneficiarios")]
    public virtual EstadoRegistro? EstadoRegistro { get; set; }

    [InverseProperty("Beneficiario")]
    public virtual ICollection<EstudiosRealizado> EstudiosRealizados { get; set; } = new List<EstudiosRealizado>();

    [InverseProperty("Beneficiario")]
    public virtual ICollection<ExperienciaLaboral> ExperienciaLaborales { get; set; } = new List<ExperienciaLaboral>();

    [ForeignKey("GeneroId")]
    [InverseProperty("Beneficiarios")]
    public virtual Genero? Genero { get; set; }

    [InverseProperty("Beneficiario")]
    public virtual ICollection<Habilidad> Habilidades { get; set; } = new List<Habilidad>();

    [InverseProperty("Beneficiario")]
    public virtual ICollection<Idioma> Idiomas { get; set; } = new List<Idioma>();

    [ForeignKey("PaisId")]
    [InverseProperty("Beneficiarios")]
    public virtual GcPais? Pais { get; set; }

    [ForeignKey("PuestoId")]
    [InverseProperty("Beneficiarios")]
    public virtual Puesto? Puesto { get; set; }
}
