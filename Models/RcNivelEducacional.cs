﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace Postulantes.Models;

[Table("rc_nivel_educacional")]
public partial class RcNivelEducacional
{
    [Key]
    [Column("nivel_educ_codigo")]
    public int NivelEducCodigo { get; set; }

    [Column("nivel_educ_descripcion")]
    [StringLength(50)]
    [Unicode(false)]
    public string? NivelEducDescripcion { get; set; }

    [JsonIgnore]
    [Column("tipo")]
    public int? Tipo { get; set; }

    [JsonIgnore]
    [Column("estado_codigo")]
    [StringLength(3)]
    [Unicode(false)]
    public string? EstadoCodigo { get; set; }

    [JsonIgnore]
    [Column("usr_codigo")]
    [StringLength(20)]
    [Unicode(false)]
    public string? UsrCodigo { get; set; }

    [JsonIgnore]
    [Column("fecha_registro", TypeName = "datetime")]
    public DateTime? FechaRegistro { get; set; }

    [JsonIgnore]
    [Column("hora_registro")]
    [StringLength(8)]
    [Unicode(false)]
    public string? HoraRegistro { get; set; }

    [JsonIgnore]
    [InverseProperty("NivelEducativo")]
    public virtual ICollection<EstudiosRealizado> EstudiosRealizados { get; } = new List<EstudiosRealizado>();
}
