﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace Postulantes.Models;

[Table("EstadoRegistro", Schema = "rrhh")]
public partial class EstadoRegistro
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int IdEstadoRegistro { get; set; }

    [StringLength(3)]
    [Unicode(false)]
    public string? EstadoCodigoId { get; set; }

    [StringLength(60)]
    [Unicode(false)]
    public string? Descripcion { get; set; }

    [JsonIgnore]
    [InverseProperty("EstadoRegistro")]
    public virtual ICollection<Beneficiario> Beneficiarios { get; } = new List<Beneficiario>();
}
