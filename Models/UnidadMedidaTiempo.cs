﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace Postulantes.Models;

[Table("UnidadMedidaTiempo", Schema = "rrhh")]
public partial class UnidadMedidaTiempo
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int IdUnidadMedida { get; set; }

    [StringLength(50)]
    [Unicode(false)]
    public string? Descripcion { get; set; }

    [JsonIgnore]
    [StringLength(5)]
    [Unicode(false)]
    public string? Codigo { get; set; }

    [JsonIgnore]
    public int? EstadoRegistroId { get; set; }

    [JsonIgnore]
    [InverseProperty("UnidadMedida")]
    public virtual ICollection<Habilidad> Habilidades { get; } = new List<Habilidad>();
}
