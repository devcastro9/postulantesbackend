﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace Postulantes.Models;

[Table("Puesto", Schema = "rrhh")]
public partial class Puesto
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int IdPuesto { get; set; }

    [StringLength(150)]
    [Unicode(false)]
    public string? Descripcion { get; set; }

    [JsonIgnore]
    public int? DepartamentoId { get; set; }

    [JsonIgnore]
    public int? EmpresaId { get; set; }

    [JsonIgnore]
    public bool? Vacante { get; set; }

    [JsonIgnore]
    public bool? Publicado { get; set; }

    [JsonIgnore]
    public int? EstadoRegistroId { get; set; }

    [JsonIgnore]
    public int? UsuarioId { get; set; }

    [JsonIgnore]
    [Column(TypeName = "datetime")]
    public DateTime? FechaRegistro { get; set; }

    [JsonIgnore]
    [InverseProperty("Puesto")]
    public virtual ICollection<Beneficiario> Beneficiarios { get; } = new List<Beneficiario>();

    [JsonIgnore]
    [ForeignKey("DepartamentoId")]
    [InverseProperty("Puestos")]
    public virtual Departamento? Departamento { get; set; }
}
