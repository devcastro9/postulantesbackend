﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace Postulantes.Models;

[Table("Departamento", Schema = "rrhh")]
public partial class Departamento
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int IdDepartamento { get; set; }

    [StringLength(50)]
    [Unicode(false)]
    public string? Descripcion { get; set; }

    [JsonIgnore]
    public bool? Habilitado { get; set; }

    [JsonIgnore]
    [InverseProperty("Departamento")]
    public virtual ICollection<Beneficiario> Beneficiarios { get; } = new List<Beneficiario>();

    [JsonIgnore]
    [InverseProperty("Departamento")]
    public virtual ICollection<Puesto> Puestos { get; } = new List<Puesto>();
}
