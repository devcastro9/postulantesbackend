﻿using Microsoft.AspNetCore.Identity;

namespace Postulantes.Models
{
    public class UsuarioNet : IdentityUser
    {
        public int DepartamentoId { get; set; }
    }
}
