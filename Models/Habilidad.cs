﻿using Microsoft.EntityFrameworkCore;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Postulantes.Models;

[Table("Habilidad", Schema = "rrhh")]
public partial class Habilidad
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int IdHabilidad { get; set; }

    public int? BeneficiarioId { get; set; }

    [StringLength(150)]
    [Unicode(false)]
    public string? Descripcion { get; set; }

    public int? Tiempo { get; set; }

    public int? UnidadMedidaId { get; set; }

    public int? EstadoRegistroId { get; set; }

    [Column(TypeName = "datetime")]
    public DateTime? FechaRegistro { get; set; }

    [ForeignKey("BeneficiarioId")]
    [InverseProperty("Habilidades")]
    public virtual Beneficiario? Beneficiario { get; set; }

    [ForeignKey("UnidadMedidaId")]
    [InverseProperty("Habilidades")]
    public virtual UnidadMedidaTiempo? UnidadMedida { get; set; }
}
