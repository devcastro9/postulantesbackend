﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace Postulantes.Models;

[Table("Genero", Schema = "rrhh")]
public partial class Genero
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int IdGenero { get; set; }

    [JsonIgnore]
    [StringLength(2)]
    [Unicode(false)]
    public string? Sigla { get; set; }

    [StringLength(50)]
    [Unicode(false)]
    public string? Descripcion { get; set; }

    [JsonIgnore]
    public int? EstadoRegistroId { get; set; }

    [JsonIgnore]
    public int? UsuarioId { get; set; }

    [JsonIgnore]
    [Column(TypeName = "datetime")]
    public DateTime? FechaRegistro { get; set; }

    [JsonIgnore]
    [InverseProperty("Genero")]
    public virtual ICollection<Beneficiario> Beneficiarios { get; } = new List<Beneficiario>();
}
