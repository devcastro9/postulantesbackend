﻿using Microsoft.EntityFrameworkCore;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Postulantes.Models;

[Table("ExperienciaLaboral", Schema = "rrhh")]
public partial class ExperienciaLaboral
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int IdExperienciaLaboral { get; set; }

    public int? BeneficiarioId { get; set; }

    [StringLength(50)]
    [Unicode(false)]
    public string? NombreInstitucion { get; set; }

    [StringLength(50)]
    [Unicode(false)]
    public string? Cargo { get; set; }

    [StringLength(500)]
    [Unicode(false)]
    public string? FuncionGeneral { get; set; }

    public int? PaisId { get; set; }

    [StringLength(50)]
    [Unicode(false)]
    public string? Ciudad { get; set; }

    [Column(TypeName = "date")]
    public DateTime? FechaInicio { get; set; }

    [Column(TypeName = "date")]
    public DateTime? FechaFinalizacion { get; set; }

    public bool? PresentoDocumento { get; set; }

    public int? EstadoRegistroId { get; set; }

    [Column(TypeName = "datetime")]
    public DateTime? FechaRegistro { get; set; }

    [ForeignKey("BeneficiarioId")]
    [InverseProperty("ExperienciaLaborales")]
    public virtual Beneficiario? Beneficiario { get; set; }

    [ForeignKey("PaisId")]
    [InverseProperty("ExperienciaLaborales")]
    public virtual GcPais? Pais { get; set; }
}
