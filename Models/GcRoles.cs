﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Postulantes.Models;

[Table("gc_roles")]
public partial class GcRoles
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int Id { get; set; }

    [StringLength(100)]
    [Unicode(false)]
    public string? Nombre { get; set; }

    [StringLength(100)]
    [Unicode(false)]
    public string? NombreNormalizado { get; set; }

    [Column("CHQ")]
    public bool? Chq { get; set; }

    [Column("LPZ")]
    public bool? Lpz { get; set; }

    [Column("CBB")]
    public bool? Cbb { get; set; }

    [Column("ORU")]
    public bool? Oru { get; set; }

    [Column("PTS")]
    public bool? Pts { get; set; }

    [Column("TJA")]
    public bool? Tja { get; set; }

    [Column("SCZ")]
    public bool? Scz { get; set; }

    [Column("BEN")]
    public bool? Ben { get; set; }

    [Column("PDO")]
    public bool? Pdo { get; set; }

    [Column("EXT")]
    public bool? Ext { get; set; }

    [InverseProperty("IdRoleNavigation")]
    public virtual ICollection<GcRight> GcRights { get; } = new List<GcRight>();

    [InverseProperty("Role")]
    public virtual ICollection<GcUsuariosRoles> GcUsuariosRoles { get; } = new List<GcUsuariosRoles>();
}
