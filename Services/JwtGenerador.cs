﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Postulantes.Models;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Postulantes.Services
{
    public class JwtGenerador : IJwtgenerador
    {
        public async Task<(string, DateTime)> CrearToken(UsuarioNet usuario, UserManager<UsuarioNet> userManager, IConfiguration config)
        {
            // Todos los roles
            IList<string> roles = await userManager.GetRolesAsync(usuario);
            // Claims
            List<Claim> claimsToken = new()
            {
                new Claim(ClaimTypes.PrimarySid, usuario.Id),
                new Claim(ClaimTypes.Sid, usuario.UserName ?? "SN"),
                new Claim(ClaimTypes.Email, usuario.Email ?? ""),
                new Claim(ClaimTypes.StateOrProvince, usuario.DepartamentoId.ToString())
            };
            if (roles.Count == 0)
            {
                return ("NoRoles", DateTime.Now);
            }
            Parallel.ForEach(roles, rol => claimsToken.Add(new Claim(ClaimTypes.Role, rol)));
            // Token
            SymmetricSecurityKey securityKey = new(Encoding.UTF8.GetBytes(config["Jwt:Key"]!));
            SigningCredentials credentials = new(securityKey, SecurityAlgorithms.HmacSha256Signature);
            DateTime expiracionFecha = DateTime.Now.AddMinutes(30);
            JwtSecurityToken tokenDescriptor = new(
                issuer: config["Jwt:Issuer"],
                //audience: config["Jwt:Audience"],
                claims: claimsToken,
                expires: expiracionFecha,
                signingCredentials: credentials);
            string jwt = new JwtSecurityTokenHandler().WriteToken(tokenDescriptor);

            return (jwt, expiracionFecha);
        }
    }
}
