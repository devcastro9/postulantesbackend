﻿using iText.Kernel.Pdf;
using iText.Layout;
using System.IO;
using System.Threading.Tasks;

namespace Postulantes.Services
{
    public class ReportePostulante
    {
        public async Task GetReportePostulante()
        {
            using MemoryStream stream = new();
            using PdfWriter writer = new(stream);
            PdfDocument pdfDocument = new(writer);
            Document documento = new(pdfDocument);
            await Task.Delay(1000);
        }
    }
}
