﻿namespace Postulantes.Services
{
    public interface IFtpServer
    {
        public bool Cargar(byte[] data, string RutaDestino);
        public byte[]? Obtener(string RutaOrigen);
    }
}
