﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Postulantes.Models;
using System;
using System.Threading.Tasks;

namespace Postulantes.Services
{
    public interface IJwtgenerador
    {
        public Task<(string, DateTime)> CrearToken(UsuarioNet usuario, UserManager<UsuarioNet> userManager, IConfiguration config);
    }
}
