﻿using FluentFTP;
using System;
using System.Security.Authentication;

namespace Postulantes.Services
{
    public class FtpServer : IFtpServer, IDisposable
    {
        private readonly string _host;
        private readonly string _user;
        private readonly string _password;
        private readonly int _port = 21;
        private FtpClient? _ftp;
        private readonly FtpConfig _config = new()
        {
            EncryptionMode = FtpEncryptionMode.Auto,
            SslProtocols = SslProtocols.Tls12,
            ValidateAnyCertificate = true,
            DataConnectionType = FtpDataConnectionType.PASV,
            RetryAttempts = 3,
            ConnectTimeout = 10000
        };
        public FtpServer(string? host, string? user, string? password)
        {
            _host = host ?? "";
            _user = user ?? "";
            _password = password ?? "";
        }

        public bool Cargar(byte[] data, string RutaDestino)
        {
            _ftp = new FtpClient(_host, _user, _password, _port, _config);
            try
            {
                _ftp.Connect();
                _ftp.UploadBytes(data, RutaDestino, FtpRemoteExists.Overwrite, false);
            }
            catch (Exception)
            {
                if (_ftp.IsConnected)
                {
                    _ftp.Disconnect();
                }
                return false;
            }
            finally
            {
                if (_ftp.IsConnected)
                {
                    _ftp.Disconnect();
                }
            }
            return true;
        }

        public byte[]? Obtener(string RutaOrigen)
        {
            byte[] imagen;
            _ftp = new FtpClient(_host, _user, _password, _port, _config);
            try
            {
                _ftp.Connect();
                _ftp.DownloadBytes(out imagen, RutaOrigen);
                _ftp.Disconnect();
            }
            catch (Exception)
            {
                //throw;
                return null;
            }
            finally
            {
                if (_ftp.IsConnected)
                {
                    _ftp.Disconnect();
                }
            }
            return imagen;
        }

        public void Dispose()
        {
            _ftp?.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}
