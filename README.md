# Backend para el sistema SOFIA
Backend para el sistema Sofia:

    1. BackEnd:

        * Lenguaje de programacion backend: C# 11

        * SDK: .NET7 v7 (version LTS vigente hasta el 14 de mayo de 2024)

        * ORM: Entity Framework Core v7 (Vigente hasta el 14 de mayo de 2024)

        * Base de datos: Postgresql v15

    2. FrontEnd

        * Manejador de versiones de NodeJS: NVM

        * NodeJS v18.14.0 (Version LTS vigente hasta el 23 de septiembre de 2023)

        * NPM v9.3.1
        
        * Angular CLI: ng v15.1.4

        * Angular v15.1.4

        * Angular Material v15.1.4

        * Librerias Adicionales: ChartJS v3.8.0 (ng2-charts), IntroJS v5.1.0 (angular-intro.js), SweetAlert2 v11.4.20 (ngx-sweetalert2)

    3. Infraestructura

        * Windows Server 2019/Ubuntu 20 (Sistema operativo del Servidor)

        * Servidor web: IIS v10 (Servidor web con proxy inverso, en caso de despliegue con Docker)

        * Docker v20 (Contenedores)

Directriz:
Esta version estara posibilitada para actualizarse a NET 8, en noviembre de 2024.

## Comando
Comando de ingenieria inversa para RRHH:
```
Scaffold-DbContext "Server=tcp:192.168.3.133,1433;Database=ADMIN_EMPRESA;Trusted_Connection=True;Encrypt=False" -Provider Microsoft.EntityFrameworkCore.SqlServer -DataAnnotations -Context AppDbContext -ContextDir Data -OutputDir Models -NoOnConfiguring -Tables Beneficiario, Departamento, EstudiosRealizados, ExperienciaLaboral, Habilidad, Idioma, Puesto, Genero, EstadoRegistro, UnidadMedidaTiempo, gc_pais, gc_tipo_documento_id, rc_estado_civil, rc_nivel_educacional -Force
```
Comando de ingenieria inversa para Seguridad:
```
Scaffold-DbContext "Server=tcp:192.168.3.133,1433;Database=ADMIN_EMPRESA;Trusted_Connection=True;Encrypt=False" -Provider Microsoft.EntityFrameworkCore.SqlServer -DataAnnotations -Context AppDbContext -ContextDir Data -OutputDir Models -NoOnConfiguring -Tables gc_usuarios,gc_roles,gc_usuarios_roles,gc_right,gc_controles,gc_formularios -Force
```
Comndo 
```
Scaffold-DbContext "Server=tcp:192.168.3.133,1433;Database=ADMIN_EMPRESA;Trusted_Connection=True;Encrypt=False" -Provider Microsoft.EntityFrameworkCore.SqlServer -DataAnnotations -Context AppDbContext -ContextDir Data -OutputDir Models -NoOnConfiguring -Tables Beneficiario, Departamento, EstudiosRealizados, ExperienciaLaboral, Habilidad, Idioma, Puesto, Genero, EstadoRegistro, UnidadMedidaTiempo, gc_pais, gc_tipo_documento_id, rc_estado_civil, rc_nivel_educacional, gc_usuarios,gc_roles,gc_usuarios_roles,gc_right,gc_controles,gc_formularios -Force
```
Reportes gerenciales:
```
echo 'Hola mundo'
```
Primera migracion:
```
Add-Migration -Name 'Primera migracion' -OutputDir Data/Migrations
```

Conectarse al contenedor:
```
sudo docker attach --sig-proxy=false name-contenedor
```