#See https://aka.ms/containerfastmode to understand how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM mcr.microsoft.com/dotnet/aspnet:7.0 AS base
ENV TZ=America/La_Paz
ENV ASPNETCORE_URLS=http://+:5032
ENV TERM=xterm-256color
WORKDIR /app
EXPOSE 5032
#EXPOSE 443

FROM mcr.microsoft.com/dotnet/sdk:7.0 AS build
WORKDIR /src
COPY ["Postulantes.csproj", "."]
RUN dotnet restore "./Postulantes.csproj"
COPY . .
WORKDIR "/src/."
RUN dotnet build "Postulantes.csproj" -c Release -o /app/build --nologo

FROM build AS publish
RUN dotnet publish "Postulantes.csproj" -c Release -o /app/publish /p:UseAppHost=false --nologo

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "Postulantes.dll"]